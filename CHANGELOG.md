# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.3](https://gitlab.com/opencontent/openagenda/compare/2.1.2...2.1.3) - 2023-08-04
- Revert "Update publiccode.yml"
- Update publiccode.yml
- Update changelog and publiccode

#### Code dependencies
| Changes                               | From     | To        | Compare                                                                                         |
|---------------------------------------|----------|-----------|-------------------------------------------------------------------------------------------------|
| aws/aws-crt-php                       | v1.0.2   | v1.2.2    | [...](https://github.com/awslabs/aws-crt-php/compare/v1.0.2...v1.2.2)                           |
| aws/aws-sdk-php                       | 3.225.0  | 3.277.8   | [...](https://github.com/aws/aws-sdk-php/compare/3.225.0...3.277.8)                             |
| composer/installers                   | af93ba6  | 2a91702   | [...](https://github.com/composer/installers/compare/af93ba6...2a91702)                         |
| firebase/php-jwt                      | v6.2.0   | v6.4.0    | [...](https://github.com/firebase/php-jwt/compare/v6.2.0...v6.4.0)                              |
| friendsofsymfony/http-cache           | 2.13.0   | 2.x-dev   | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/2.13.0...2.x-dev)                |
| google/apiclient                      | 5738766  | v2.14.0   | [...](https://github.com/googleapis/google-api-php-client/compare/5738766...v2.14.0)            |
| google/apiclient-services             | v0.252.0 | v0.302.0  | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.252.0...v0.302.0) |
| google/auth                           | v1.21.0  | v1.26.0   | [...](https://github.com/googleapis/google-auth-library-php/compare/v1.21.0...v1.26.0)          |
| guzzlehttp/promises                   | fe752ae  | 1.5.x-dev | [...](https://github.com/guzzle/promises/compare/fe752ae...1.5.x-dev)                           |
| guzzlehttp/psr7                       | 1.x-dev  | 1.9.x-dev | [...](https://github.com/guzzle/psr7/compare/1.x-dev...1.9.x-dev)                               |
| opencontent/ocbootstrap-ls            | 1.10.13  | 1.10.14   | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.13...1.10.14)                 |
| opencontent/oci18n                    | c67c37e  | e34b8d7   | [...](https://github.com/OpencontentCoop/oci18n/compare/c67c37e...e34b8d7)                      |
| opencontent/ocinstaller               | 5fd5bdd  | 36d45d6   | [...](https://github.com/OpencontentCoop/ocinstaller/compare/5fd5bdd...36d45d6)                 |
| opencontent/ocopendata-ls             | 2.28.1   | 2.86.7    | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.28.1...2.86.7)                    |
| opencontent/ocopendata_forms-ls       | 1.6.11   | 1.6.13    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.11...1.6.13)              |
| opencontent/ocrecaptcha-ls            | 1.5      | 1.5.1     | [...](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.5...1.5.1)                       |
| opencontent/ocsocialuser-ls           | 1.8.1    | 1.8.2     | [...](https://github.com/OpencontentCoop/ocsocialuser/compare/1.8.1...1.8.2)                    |
| opencontent/ocsupport-ls              | 848dad7  | 46e1759   | [...](https://github.com/OpencontentCoop/ocsupport/compare/848dad7...46e1759)                   |
| opencontent/openpa-ls                 | 085a112  | 9200a5c   | [...](https://github.com/OpencontentCoop/openpa/compare/085a112...9200a5c)                      |
| opencontent/openpa_agenda-ls          | 1.28.3   | 1.28.6    | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.28.3...1.28.6)                 |
| opencontent/openpa_bootstrapitalia-ls | 69228fa  | c76d49b   | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/69228fa...c76d49b)      |
| opencontent/openpa_theme_2014-ls      | 2.16.0   | 2.16.5    | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.16.0...2.16.5)             |
| paragonie/constant_time_encoding      | v2.5.0   | v2.6.3    | [...](https://github.com/paragonie/constant_time_encoding/compare/v2.5.0...v2.6.3)              |
| php-http/client-common                | 2.5.0    | 2.x-dev   | [...](https://github.com/php-http/client-common/compare/2.5.0...2.x-dev)                        |
| php-http/discovery                    | 1.14.2   | 1.x-dev   | [...](https://github.com/php-http/discovery/compare/1.14.2...1.x-dev)                           |
| php-http/httplug                      | f640739  | 2.x-dev   | [...](https://github.com/php-http/httplug/compare/f640739...2.x-dev)                            |
| php-http/message                      | 1.13.0   | 1.x-dev   | [...](https://github.com/php-http/message/compare/1.13.0...1.x-dev)                             |
| php-http/message-factory              | 597f30e  | 4d8778e   | [...](https://github.com/php-http/message-factory/compare/597f30e...4d8778e)                    |
| predis/predis                         | 99c2537  | v2.x-dev  | [...](https://github.com/predis/predis/compare/99c2537...v2.x-dev)                              |
| psr/event-dispatcher                  | aa4f89e  | e275e2d   | [...](https://github.com/php-fig/event-dispatcher/compare/aa4f89e...e275e2d)                    |
| psr/http-client                       | 22b2ef5  | 0955afe   | [...](https://github.com/php-fig/http-client/compare/22b2ef5...0955afe)                         |
| psr/http-factory                      | 36fa03d  | 6d70f40   | [...](https://github.com/php-fig/http-factory/compare/36fa03d...6d70f40)                        |
| psr/http-message                      | efd67d1  | 1.1       | [...](https://github.com/php-fig/http-message/compare/efd67d1...1.1)                            |
| symfony/polyfill-ctype                | 6fd1b9a  | 1.x-dev   | [...](https://github.com/symfony/polyfill-ctype/compare/6fd1b9a...1.x-dev)                      |
| symfony/polyfill-intl-idn             | 59a8d27  | 1.x-dev   | [...](https://github.com/symfony/polyfill-intl-idn/compare/59a8d27...1.x-dev)                   |
| symfony/polyfill-intl-normalizer      | 219aa36  | 1.x-dev   | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/219aa36...1.x-dev)            |
| symfony/polyfill-mbstring             | 9344f9c  | 1.x-dev   | [...](https://github.com/symfony/polyfill-mbstring/compare/9344f9c...1.x-dev)                   |
| symfony/polyfill-php72                | bf44a9f  | 1.x-dev   | [...](https://github.com/symfony/polyfill-php72/compare/bf44a9f...1.x-dev)                      |
| symfony/polyfill-php73                | e440d35  | 1.x-dev   | [...](https://github.com/symfony/polyfill-php73/compare/e440d35...1.x-dev)                      |
| symfony/polyfill-php80                | cfa0ae9  | 1.x-dev   | [...](https://github.com/symfony/polyfill-php80/compare/cfa0ae9...1.x-dev)                      |
| zetacomponents/archive                | 1.5      | 1.5.1     | [...](https://github.com/zetacomponents/Archive/compare/1.5...1.5.1)                            |
| zetacomponents/base                   | 1.9.3    | 1.9.4     | [...](https://github.com/zetacomponents/Base/compare/1.9.3...1.9.4)                             |
| zetacomponents/cache                  | 1.6.1    | 1.6.2     | [...](https://github.com/zetacomponents/Cache/compare/1.6.1...1.6.2)                            |
| zetacomponents/database               | 1.5.2    | 1.5.3     | [...](https://github.com/zetacomponents/Database/compare/1.5.2...1.5.3)                         |
| zetacomponents/mail                   | 1.9.2    | 1.9.4     | [...](https://github.com/zetacomponents/Mail/compare/1.9.2...1.9.4)                             |
| zetacomponents/mvc-tools              | 1.2.1    | 1.2.2     | [...](https://github.com/zetacomponents/MvcTools/compare/1.2.1...1.2.2)                         |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.13 and 1.10.14](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.13...1.10.14)**
* Avoid google recaptcha marketing cookies

**[opencontent/oci18n changes between c67c37e and e34b8d7](https://github.com/OpencontentCoop/oci18n/compare/c67c37e...e34b8d7)**
* Add csv->class util script

**[opencontent/ocinstaller changes between 5fd5bdd and 36d45d6](https://github.com/OpencontentCoop/ocinstaller/compare/5fd5bdd...36d45d6)**
* Minor bug fixes
* Add missing tag description setter in tag from csv
* Add sync with local feature
* Add node_id_from_remote_id function var
* Avoid missing node error Force regenerate class cache
* Add reset content fields
* Avoid published version duplication
* Allow override installer var invoking script
* Add lock status feature
* Remove tag only from custom script
* Remove verbose debug log
* Fix merge imports
* Store data dit foreach version
* Bugfix in TagTreeCsv installer
* Add slack notification
* Fix contentree payload builder
* Fix slack integration
* Typo fix
* Add prebuilt installer Bugfix in main bin

**[opencontent/ocopendata-ls changes between 2.28.1 and 2.86.7](https://github.com/OpencontentCoop/ocopendata/compare/2.28.1...2.86.7)**
* Fix block items priority in set Page api
* Fix empty field validation
* Add cache-control response filter (disabled by default)
* Fix file name enconding in api file url Bugfix in PAge connector
* Encode filename in file.url payload if needed
* Fix eztags attribute converter

**[opencontent/ocopendata_forms-ls changes between 1.6.11 and 1.6.13](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.11...1.6.13)**
* Corregge un problema di compatibilità di bootstrap nella navigazione a tab dei form
* Fix Nominatim api paths

**[opencontent/ocrecaptcha-ls changes between 1.5 and 1.5.1](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.5...1.5.1)**
* Avoid google recaptcha marketing cookies

**[opencontent/ocsocialuser-ls changes between 1.8.1 and 1.8.2](https://github.com/OpencontentCoop/ocsocialuser/compare/1.8.1...1.8.2)**
* Corregge alcune stringhe di traduzione in greco
* Avoid google recaptcha marketing cookies

**[opencontent/ocsupport-ls changes between 848dad7 and 46e1759](https://github.com/OpencontentCoop/ocsupport/compare/848dad7...46e1759)**
* Add push_var_to_s3 script to migrate storage from nfs to s3
* Add migration utils script

**[opencontent/openpa-ls changes between 085a112 and 9200a5c](https://github.com/OpencontentCoop/openpa/compare/085a112...9200a5c)**
* Permette di configurare lo storage del cluster della cache su nfs e della var/storage in S3-like
* Corregge un problema di lettura delle configurazioni nel clustering in nfs
* Include il remote id nella generazione dei tree menu
* Aggiunge nuove voci nella matrice dei contatti
* Permette di configurare un endpoint custom per servire le immagini da AWS S3
* Permette di configurare un firewall per l'invio mail basato su bounce collector
* Introduce un cron dedicato all'aggiornamento delle liste di newsletter in base al bounce collector se configurato
* Fix Nominatim api paths

**[opencontent/openpa_agenda-ls changes between 1.28.3 and 1.28.6](https://github.com/OpencontentCoop/openpa_agenda/compare/1.28.3...1.28.6)**
* Avoid google recaptcha marketing cookies
* Eredita il cookie consent da openpa_bootstrapitalia
* Add cookie preferences in footer menu

**[opencontent/openpa_bootstrapitalia-ls changes between 69228fa and c76d49b](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/69228fa...c76d49b)**
* Permette di specificare una limitazione di sotto alberatura nella definizione degli attributi di tipo 'Visualizzazione degli oggetti correlati inversi'
* Avoid google recaptcha marketing cookies
* Aggiunge l'attributo target al banner (se abilitato)
* Visualizza le date nella newsletter solo per i contenuti di classe event
* Imposta come default il cookie consente avanzato
* Corregge un problema di internazionalizzazione delle date nelle tabelle della trasparenza
* Introduce un endpoint openapi dedicato alle pagine dell'amministrazione trasparente
* Corregge un problema sul campo lotto/id nell'esposizione in openapi Introduce un endpoint openapi dedicato ai lotti
* Corregge un bug nella selezione dei filtri di ricerca
* Introduce un nuovo design compatibile con la versione 2  di bootstrapitalia
* Introduce una versione mock del widget di customer satisfaction
* Corregge alcune impostazioni dei temi css
* Introduce un nuovo design compatibile con la versione 2  di bootstrapitalia
* Introduce una versione mock del widget di customer satisfaction
* Corregge alcune impostazioni dei temi css
* no message
* Corregge alcuni bug di visualizzazione dell'indice della pagina Introduce un data handler per la visualizzazione dei meta info per stanzadelcittadino
* Introduce i tasti di navigazione nella vista edit Corregge un bug di visualizzazione di un link al numero di telefono
* Permette di configurare un entrypoint satisfy dalla dashboard di valutazione
* Minor bug fixes
* Corregge la visualizzazione dei default dei topic
* Introduce un sistema di autodiscover per ottenere le informazione del profilo
* Modifica la visualizzazione di default degli elementi figli
* Introduce la configurazione AutoDiscoverProfileLinks
* Visualizza il form di iscrizione alla newsletter nel footer
* Corregge la visualizzazione di default degli argomenti
* Aggiorna le dipendenze di bootstrapitalia alla versione 2.2.0
* Corregge la formattazione della pagina FAQ
* Introduce le configurazioni per le app built-in
* Corregge la configurazione di build per script principale bootstrapitalia
* Rimuove il target blank per gli url di accesso al servizio
* Aggiorna le stringhe di traduzione
* Corregge un bug di visualizzazione nei contatti del footer Impedisce l'upload contestuale delle immagini
* Aggiunge l'accesso alle bozze Nasconde gli argomenti senza relazioni
* Corregge un problema di cache nel footer
* Carica i font il prima possibile
* Load fonts via JS
* Rimuove le icone nel xmltag link
* Ottimizzazione dei javascript inclusi
* Introduce il bottone per impersonificare l'utente in settings utente
* Corregge un problema di visualizzazione delle immagini nelle card
* Rimuove la stringa statica "Servizio" nello stato del public_service
* Corregge un problema di compatibilità di bootstrap nel widget opendatabrowse
* Imposta l'url relativo per i datatable delle pagine trasparenza
* Nelle dashboard redazionali viene invocato il webhook postpublish al cambio stato
* Corregge un bug per cui le faq non venivano visualizzate nella lingua corretta
* Corregge un bug di visualizzazione del calendario remoto
* Corregge un bug per cui i link dei contenuti correlati non erano corretti
* Forza la visualizzazione card simple per la libreria media
* Corregge il caricamento degli asset css e js per alleggerire il caricamento della pagina
* Corregge la selezione dei banner colorati per ottimizzare il caricamento dei css
* Corregge lo script di inclusione di satisfy
* Rimuove gli spazi in eccesso dall'html prodotto
* Specifica le dimensioni del logo eu nel footer
* Corregge un problema di caricamento delle librerie leaflet
* Riduce le dimensioni dell'immagine principale nel blocco singolo in versione mobile
* Corregge la visualizzazione della mappa nei luoghi e negli eventi
* Corregge un bug javascript nella visualizzazione della mappa
* Corregge un bug javascript nella visualizzazione della mappa negli eventi
* Corregge un bug javascript nella visualizzazione della mappa negli eventi
* Permette di nascondere il blocco contatti da configurazione
* Corregge il path per recuperare la sessione da area personale
* Corregge il path per recuperare la sessione da area personale
* Corregge il path per recuperare la sessione da area personale
* Corregge una regression nel blocco calendari remoti
* Espone un metodo di migrazione dei banner colors
* Permette di nascondere lo slim header da configurazione
* Aggiorna i data-element secondo quanto previsto da https://github.com/italia/app-valutazione-modelli-docs@42bc2a8
* Corregge un bug di visualizzazione nel blocco remoto e nel blocco argomenti
* Corregge un problema di accessibilità nel cookie consent
* Rimuove il caricamento di default di Jquery UI
* Rimuove il data-element="feedback" se presente il widget di satisfy
* Inserisce il data-element legal-notes-section Corregge alcuni errori di esposizione dei data-element
* Corregge alcuni bug di visualizzazione
* Introduce la splash page accedi e un sistema di connessione automatica con Stanzadelcittadino
* Corregge alcuni problemi di accessibiità nel cookieconsent
* Fix accessibility issues in gallery ui
* Corregge l'url di logout dell'utente remoto
* Riduce e riorganizza il codice javascript caricato di default
* Inserisce il testo di default per la licenza di contenuti Preconfigura i servizi builtin in base
* Corregge il data-element di note legali
* Corregge un errore grave nella definizione della app builtin
* Corregge un problema di configurazione delle built in
* Aggiunge una vista a card per il blocco calendario
* Corregge alcune visualizzazioni e alcuni bug Introduce una libreria di utilità per l’installer
* Permette di visualizzare i contenuti su tag oltre al secondo livello Impedisce di mostrare contenuti non corretamente collocati
* Corregge il limite nel blocco eventi con vista card
* Corregge una regression introdotta con la modifica delle visualizzazioni degli elementi figli
* Corregge un typo nella chiamata al tenant info (bridge con area personale)
* Corregge la formattazione degli attributi ezauthor
* Corregge alcuni errori che producevano warning
* Corregge i permessi di accesso al menu rapido Le mie bozze
* Permette di configurare il filr src di satisfy da configurazioni ini
* Introduce un modulo per visualizzare l'elenco delle segnalazioni del servizio builtin inefficiency
* Corregge un bug di visualizzazione nella pagina argomento per cui non venivano caricati correttamente i contenuti automatici Introduce lun’interfaccia custom per il redattore struttura sito
* Aggiunge il preload su css Serve i font da un indirizzo statico
* Introduce il caricamento lazy delle immagini Esegue il preload delle immagini di background, dello script principale dei css e dei fonts
* Aggiunge un placeholder alle immagini lazy
* Revert "Aggiunge un placeholder alle immagini lazy"  This reverts commit 0c2ddc164f6315ad5386dd6f0b80046719de10d9.
* Revert "Revert "Aggiunge un placeholder alle immagini lazy""  This reverts commit 0429fd194a46f6359800cda5c665fa5e8893aa8d.
* Introduce il srcset e data-srcset le image responsive
* Corregge il caricamento del logo e razionalizza i preload Fissa l'altezza dell'immagine del blocco singolo Corregge la query di eventi cards Corregge il caricamento del logo Rimuove un'icona font-awesome dalla scheda del servizio
* Corregge un bug nel logo
* Corregge un problema nel blocco eventi_card
* Corregge un type nel blocco singolo
* Aggiorna le dipendenze del widget "Segnala disservizio"
* disable FlyImg prefix image by default
* Corregge alcuni bug nella definizione dell'url delle immagini Corregge alcuni bug nell'interfaccia locked Permette di modificar eil logo in impostazioni info
* Corregge un errore nella modifica delle info generali
* Introduce il nuovo permesso Editor Homepage e aggiunge i privilegi di modifica delle pagine del sito in ciascun permesso di sezione
* Corregge alcuni bug di visualizzazione
* Corregge un problema di lingua dell'editor pagine sito
* Aggiunge il link all'assistente migrazione nella toolbar
* Visualizza le liste del personale come card teaser
* Permette di svuotare il contenuto di un attributo openparole impostando a zero la paginazione
* Corregge il testo di default delle specifiche di licenza in conformità ala validatore
* Corregge un bug nella paginazione dell'attributo openparole
* Visualizza il link "Ulteriori dettagli" nella visualizzazione card_teaser_info qualora il titolo sia nascosto
* Abilita l'AnnounceKit di default
* Corregge un problema di visualizzazione della tabella di openparole
* Introduce alcuni miglioramenti dell'interfaccia permette di visualizzare l'anteprima dell'immagine quando si seleziona da libreria correggi alcune stringhe non tradotte corregge il limite di siti tematici notizie e argomenti nell'interfaccia di modifica
* Introduce una splash page per l'accesso riservato allo staff
* Permette di modificare la favicon dalla nuova interfaccia di gestione delle informazioni principali
* Rimuove l'accesso al backend dalla website toolbar Corregge una stringa traducibile
* Abilita di default le openapi trasaprenza
* Introduce la visualizzazione per gli oggetti di classe Documento trasparenza
* Corregge un bug di visualizzazione della modifica dei gruppi di attributi Espone il link nel card_teaser per le classi non configurate
* Permette di nascondere tramite configurazione i topics dall'overview della visualizzazione full
* Corregge un errore grave nel calcolo dei data-element
* Corregge un problema sulla visualizzazione dei ruoli Introduce un meccanismo di importazione delle informazioni generali
* Corregge un errore nello script di importazione delle informazioni del sito
* Aggiorna le stringhe di traduzione
* Aggiorna il meccanismo automatico di caricamento dei servizi builtin
* Introduce un manifest.json minimo
* Evita un errore in creazione delle openapi della trasparenza
* Visualizza l'elenco dei cookie tecnici esposti dal sistema (wip traduzioni)
* Sovrascrive il logo in importazione siteinfo
* Corregge l'indirizzo di logout per accesso operatori
* Introduce uno script per la creazione di utenti redattori
* Revert "Visualizza l'elenco dei cookie tecnici esposti dal sistema (wip traduzioni)"  This reverts commit d92155f9e9ac333d2427f7d64e99d5cd5a363d25.
* Corregge un bug nel blocco eventi card
* Rimuove per gli utenti loggati l'installazione del service worker per incompatibilità con l'interfaccia di edit
* Corregge un bug di visualizzazione del blocco eventi card
* Corregge la visualizzazione del topic Corregge un bug del blocco eventi
* Controlla l'effettiva abilitazione del servizio builtin prima di renderizzarlo
* Migliora l'usabilità dell'interfaccia editor contenuto quando si clicca su un bottone per avviare un'azione custom
* Esponde le informazioni generali del tenant in  /openpa/data/meta
* Preimposta un'esportazione rss di default degli articoli
* Introduce una vista dedicata per tempi e scadenze compatibile con gli shared links
* Rifattorizza la visualizzazione del copyright Introduce un pannello di impostazioni avanzate ad uso dell'admin per sincronizzare header e footer in area personale, impostare il partner, importare le informazioni da altra istanza opencityt
* Permette di rimuovere il partner configurato
* Riduce la dimensione massima delle immagini visualizzate
* Corregge l'icona di default nel manifest.json
* Permette l'ordinamento di oggetti in stato opencity_locked
* Corregge un errore di rendering del menu nella visualizzazione di una versione
* Permette di modificare i link nell'header e nel footer da gestione informazioni generali
* Introduce un pannello per la sincronizzazione dei servizi pubblici (ad uso amministrativo)
* Corregge un problema di doppia collocazione in creazione servizio da service tools
* Introduce il nuovo tema amalfi
* Rimuove il service worker di default
* Ottimizza il caricamento del tools sync servizi
* Corregge il testo guida di tools sync servizi
* Corregge un bug di visualizzazione della barra dei menu e del widget satisfy
* Perfeziona le servicetools api
* Corregge un possibile fallimento di servicetools
* Espone un controllo dei campi obbligatori sulla vista elenco dei contenuti per classe
* Effettua un controllo automatico per esporre i bottoni di accesso al servizio in base allo stato
* Introduce uno script per correggere l'errata importazione del tag "Salute, benessere e assistenza"
* Espone i contenuti correlati nella vista classlist
* Corregge il controllo dei contenuti nella vista classlist
* Corregge un bug nella vista classlist
* Introduce una api per modificare il link all'area personale e i relativi link
* Aggiunge un testo di default nel paragrafo Tempi e scadenze della scheda del servizio in caso di valori non popolati
* Permette di reindirizzare a nominatim invece che a google per il link alle geolocalizzazioni
* Permette di inserire un logo custom per il footer (se configurato homepage/footer_logo)
* Rimuove i caratteri ascii nella visualizzazione del nome del file
* Aggiunge il link al Piano di miglioramento delle performance nel footer
* Valida il campo "Motivo dello stato" in creazione o modifica di un servizio non attivo
* Forza la visualizzazione del menu utente anche in caso di link all'area riservata errato o mancante
* Corregge la minificazione dell'output dell'html
* Considera la tipologia di attributo per il campo numero nel blocco ricerca documenti (bootstrapitalia 1)
* Introduce la gestione dei servizi bis in importazione servizio da prototipo
* Corregge un bug nelle api sync servizi
* Permette di ripristinare una versione precedente di un contenuto locked
* Corregge un bug di visualizzazione del marker popup
* Corregge il colore dello slim header nello stile amalfi

**[opencontent/openpa_theme_2014-ls changes between 2.16.0 and 2.16.5](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.16.0...2.16.5)**
* Avoid google recaptcha marketing cookies
* Remove fonts.googleapis.com  and fonts.gstatic.com external calls
* Add font awesome fonts
* Fix Nominatim api paths
* Fix Nominatim api paths


## [2.1.2](https://gitlab.com/opencontent/openagenda/compare/2.1.1...2.1.2) - 2022-06-09



#### Installer
- Fix default logo source url

#### Code dependencies
| Changes                               | From     | To       | Compare                                                                                         |
|---------------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.216.0  | 3.225.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.216.0...3.225.0)                             |
| clue/stream-filter                    | v1.6.0   | 1.x-dev  | [...](https://github.com/clue/stream-filter/compare/v1.6.0...1.x-dev)                           |
| composer/installers                   | 75e5ef0  | af93ba6  | [...](https://github.com/composer/installers/compare/75e5ef0...af93ba6)                         |
| firebase/php-jwt                      | v5.5.1   | v6.2.0   | [...](https://github.com/firebase/php-jwt/compare/v5.5.1...v6.2.0)                              |
| google/apiclient                      | c0ae314  | 5738766  | [...](https://github.com/googleapis/google-api-php-client/compare/c0ae314...5738766)            |
| google/apiclient-services             | v0.240.0 | v0.252.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.240.0...v0.252.0) |
| google/auth                           | v1.18.0  | v1.21.0  | [...](https://github.com/googleapis/google-auth-library-php/compare/v1.18.0...v1.21.0)          |
| monolog/monolog                       | f2f66cd  | 2.x-dev  | [...](https://github.com/Seldaek/monolog/compare/f2f66cd...2.x-dev)                             |
| opencontent/ocinstaller               | 828b25d  | 5fd5bdd  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/828b25d...5fd5bdd)                 |
| opencontent/ocsocialdesign-ls         | 1.6.3    | 1.6.5    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.3...1.6.5)                  |
| opencontent/openpa-ls                 | 0de25d1  | 085a112  | [...](https://github.com/OpencontentCoop/openpa/compare/0de25d1...085a112)                      |
| opencontent/openpa_agenda-ls          | 1.28.2   | 1.28.3   | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.28.2...1.28.3)                 |
| opencontent/openpa_bootstrapitalia-ls | 6a641ce  | 69228fa  | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/6a641ce...69228fa)      |
| opencontent/openpa_theme_2014-ls      | 2.15.4   | 2.16.0   | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.4...2.16.0)             |
| php-http/discovery                    | 1.14.1   | 1.14.2   | [...](https://github.com/php-http/discovery/compare/1.14.1...1.14.2)                            |
| predis/predis                         | b3a5bdf  | 99c2537  | [...](https://github.com/predis/predis/compare/b3a5bdf...99c2537)                               |
| symfony/polyfill-ctype                | v1.25.0  | 6fd1b9a  | [...](https://github.com/symfony/polyfill-ctype/compare/v1.25.0...6fd1b9a)                      |
| symfony/polyfill-intl-idn             | v1.25.0  | 59a8d27  | [...](https://github.com/symfony/polyfill-intl-idn/compare/v1.25.0...59a8d27)                   |
| symfony/polyfill-intl-normalizer      | v1.25.0  | 219aa36  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/v1.25.0...219aa36)            |
| symfony/polyfill-mbstring             | v1.25.0  | 9344f9c  | [...](https://github.com/symfony/polyfill-mbstring/compare/v1.25.0...9344f9c)                   |
| symfony/polyfill-php72                | v1.25.0  | bf44a9f  | [...](https://github.com/symfony/polyfill-php72/compare/v1.25.0...bf44a9f)                      |
| symfony/polyfill-php73                | v1.25.0  | e440d35  | [...](https://github.com/symfony/polyfill-php73/compare/v1.25.0...e440d35)                      |
| symfony/polyfill-php80                | v1.25.0  | cfa0ae9  | [...](https://github.com/symfony/polyfill-php80/compare/v1.25.0...cfa0ae9)                      |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 828b25d and 5fd5bdd](https://github.com/OpencontentCoop/ocinstaller/compare/828b25d...5fd5bdd)**
* Add timestamp type in postgres dba
* Add only-schema option

**[opencontent/ocsocialdesign-ls changes between 1.6.3 and 1.6.5](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.3...1.6.5)**
* Rimuove il cookie alert dal design di default
* Installa il GA script cookieless di default

**[opencontent/openpa-ls changes between 0de25d1 and 085a112](https://github.com/OpencontentCoop/openpa/compare/0de25d1...085a112)**
* Permette la configurazione cookieless degli analytcs
* Espone un sitemap.xml essenziale

**[opencontent/openpa_agenda-ls changes between 1.28.2 and 1.28.3](https://github.com/OpencontentCoop/openpa_agenda/compare/1.28.2...1.28.3)**
* Installa il GA script cookieless di default

**[opencontent/openpa_bootstrapitalia-ls changes between 6a641ce and 69228fa](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/6a641ce...69228fa)**
* Eredita la configurazione cookie-less degli script analytics

**[opencontent/openpa_theme_2014-ls changes between 2.15.4 and 2.16.0](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.4...2.16.0)**
* Permette la configurazione cookieless degli analytcs Aggiunge lo snippet webanalytics.italia se configurato


## [2.1.1](https://gitlab.com/opencontent/openagenda/compare/2.1.0...2.1.1) - 2022-03-23
- Update to composer v2 Security fix https://security.snyk.io/vuln/SNYK-PHP-GUZZLEHTTPPSR7-2431148

#### Code dependencies
| Changes                               | From        | To          | Compare                                                                                         |
|---------------------------------------|-------------|-------------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.192.0     | 3.216.0     | [...](https://github.com/aws/aws-sdk-php/compare/3.192.0...3.216.0)                             |
| clue/stream-filter                    | v1.5.0      | v1.6.0      | [...](https://github.com/clue/stream-filter/compare/v1.5.0...v1.6.0)                            |
| composer/installers                   | 8669edf     | 75e5ef0     | [...](https://github.com/composer/installers/compare/8669edf...75e5ef0)                         |
| ezsystems/ezpublish-legacy            | 2020.1000.6 | 2020.1000.7 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.6...2020.1000.7)        |
| ezsystems/ezpublish-legacy-installer  | 38e402c     | d803123     | [...](https://github.com/Opencontent/ezpublish-legacy-installer/compare/38e402c...d803123)      |
| firebase/php-jwt                      | v5.4.0      | v5.5.1      | [...](https://github.com/firebase/php-jwt/compare/v5.4.0...v5.5.1)                              |
| friendsofsymfony/http-cache           | a501495     | 2.13.0      | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/a501495...2.13.0)                |
| google/apiclient                      | 3a98175     | c0ae314     | [...](https://github.com/googleapis/google-api-php-client/compare/3a98175...c0ae314)            |
| google/apiclient-services             | v0.211.0    | v0.240.0    | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.211.0...v0.240.0) |
| guzzlehttp/promises                   | c1dd809     | fe752ae     | [...](https://github.com/guzzle/promises/compare/c1dd809...fe752ae)                             |
| monolog/monolog                       | f2156cd     | f2f66cd     | [...](https://github.com/Seldaek/monolog/compare/f2156cd...f2f66cd)                             |
| opencontent/googlesheet               | 63b7197     | 7d19be0     | [...](https://github.com/OpencontentCoop/googlesheet/compare/63b7197...7d19be0)                 |
| opencontent/ocbootstrap-ls            | 1.10.9      | 1.10.13     | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.9...1.10.13)                  |
| opencontent/ocembed-ls                | 1.4.2       | 1.5.3       | [...](https://github.com/OpencontentCoop/ocembed/compare/1.4.2...1.5.3)                         |
| opencontent/ocinstaller               | 3a84656     | 828b25d     | [...](https://github.com/OpencontentCoop/ocinstaller/compare/3a84656...828b25d)                 |
| opencontent/ocopendata-ls             | 2.25.7      | 2.28.1      | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.7...2.28.1)                    |
| opencontent/ocsearchtools-ls          | 1.11.1      | 1.11.2      | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.1...1.11.2)                 |
| opencontent/ocsupport-ls              | 8071c61     | 848dad7     | [...](https://github.com/OpencontentCoop/ocsupport/compare/8071c61...848dad7)                   |
| opencontent/ocwebhookserver-ls        | 069a0c3     | 1.1.5       | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/069a0c3...1.1.5)               |
| opencontent/openpa-ls                 | 6272733     | 0de25d1     | [...](https://github.com/OpencontentCoop/openpa/compare/6272733...0de25d1)                      |
| opencontent/openpa_agenda-ls          | 1.26.0      | 1.28.2      | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.26.0...1.28.2)                 |
| opencontent/openpa_bootstrapitalia-ls | f47dae6     | 6a641ce     | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/f47dae6...6a641ce)      |
| paragonie/constant_time_encoding      | v2.4.0      | v2.5.0      | [...](https://github.com/paragonie/constant_time_encoding/compare/v2.4.0...v2.5.0)              |
| php-http/client-common                | 2.4.0       | 2.5.0       | [...](https://github.com/php-http/client-common/compare/2.4.0...2.5.0)                          |
| php-http/discovery                    | 1.14.0      | 1.14.1      | [...](https://github.com/php-http/discovery/compare/1.14.0...1.14.1)                            |
| php-http/httplug                      | 191a0a1     | f640739     | [...](https://github.com/php-http/httplug/compare/191a0a1...f640739)                            |
| php-http/message                      | 1.12.0      | 1.13.0      | [...](https://github.com/php-http/message/compare/1.12.0...1.13.0)                              |
| predis/predis                         | d72f067     | b3a5bdf     | [...](https://github.com/predis/predis/compare/d72f067...b3a5bdf)                               |
| symfony/deprecation-contracts         | 6f981ee     | 2.5.x-dev   | [...](https://github.com/symfony/deprecation-contracts/compare/6f981ee...2.5.x-dev)             |
| symfony/polyfill-ctype                | 46cd957     | v1.25.0     | [...](https://github.com/symfony/polyfill-ctype/compare/46cd957...v1.25.0)                      |
| symfony/polyfill-intl-idn             | 65bd267     | v1.25.0     | [...](https://github.com/symfony/polyfill-intl-idn/compare/65bd267...v1.25.0)                   |
| symfony/polyfill-intl-normalizer      | 8590a5f     | v1.25.0     | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/8590a5f...v1.25.0)            |
| symfony/polyfill-mbstring             | 9174a3d     | v1.25.0     | [...](https://github.com/symfony/polyfill-mbstring/compare/9174a3d...v1.25.0)                   |
| symfony/polyfill-php72                | 9a14221     | v1.25.0     | [...](https://github.com/symfony/polyfill-php72/compare/9a14221...v1.25.0)                      |
| symfony/polyfill-php73                | fba8933     | v1.25.0     | [...](https://github.com/symfony/polyfill-php73/compare/fba8933...v1.25.0)                      |
| symfony/polyfill-php80                | 1100343     | v1.25.0     | [...](https://github.com/symfony/polyfill-php80/compare/1100343...v1.25.0)                      |
| symfony/yaml                          | 5f947e5     | 5.4.x-dev   | [...](https://github.com/symfony/yaml/compare/5f947e5...5.4.x-dev)                              |
| zetacomponents/cache                  | 1.6         | 1.6.1       | [...](https://github.com/zetacomponents/Cache/compare/1.6...1.6.1)                              |
| zetacomponents/console-tools          | 1.7.2       | 1.7.3       | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7.2...1.7.3)                     |
| zetacomponents/database               | 1.5.1       | 1.5.2       | [...](https://github.com/zetacomponents/Database/compare/1.5.1...1.5.2)                         |
| zetacomponents/feed                   | 1.4         | 1.4.2       | [...](https://github.com/zetacomponents/Feed/compare/1.4...1.4.2)                               |


Relevant changes by repository:

**[opencontent/googlesheet changes between 63b7197 and 7d19be0](https://github.com/OpencontentCoop/googlesheet/compare/63b7197...7d19be0)**
* Update composer.json

**[opencontent/ocbootstrap-ls changes between 1.10.9 and 1.10.13](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.9...1.10.13)**
* Corregge un bug nel formato delle date prodotte dal calendario di supporto di ocevent
* Aggiorna le stringhe di traduzione
* Corregge alcune traduzioni in tedesco
* Corregge un bug di visualizzazione del template di modifica di ezcountry

**[opencontent/ocembed-ls changes between 1.4.2 and 1.5.3](https://github.com/OpencontentCoop/ocembed/compare/1.4.2...1.5.3)**
* Rifattorizza il sistema di cache Introduce un ezpfilter per il contenuto html Introduce un modulo di preview
* Fix bug in autoembed
* Verify max resolution image in youtube workaround
* php 5.6 bc

**[opencontent/ocinstaller changes between 3a84656 and 828b25d](https://github.com/OpencontentCoop/ocinstaller/compare/3a84656...828b25d)**
* Avoid workflow override

**[opencontent/ocopendata-ls changes between 2.25.7 and 2.28.1](https://github.com/OpencontentCoop/ocopendata/compare/2.25.7...2.28.1)**
* Add delete and move api
* Add upsert api
* Fix http status code in exceptions
* Change base attribute validation (from string to scalar)
* Reload modules in ContentRepository
* Hotfix reloading modules
* Fix user attribute converter on disabled user
* Rebuild parent nodes according to payload params in api update Avoid duplicate locations in api move
* Use local filesystem in file storage for performance issue

**[opencontent/ocsearchtools-ls changes between 1.11.1 and 1.11.2](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.1...1.11.2)**
* Avoid php warning

**[opencontent/ocsupport-ls changes between 8071c61 and 848dad7](https://github.com/OpencontentCoop/ocsupport/compare/8071c61...848dad7)**
* Fix github auth api call Add from to options to make-changeòpg
* Add run cronjob handler
* enable view cache in cron sqli handler
* Add reindex script

**[opencontent/ocwebhookserver-ls changes between 069a0c3 and 1.1.5](https://github.com/OpencontentCoop/ocwebhookserver/compare/069a0c3...1.1.5)**
* Add job stats

**[opencontent/openpa-ls changes between 6272733 and 0de25d1](https://github.com/OpencontentCoop/openpa/compare/6272733...0de25d1)**
* Avoid php 7.3 warning
* Remove unused code
* Allow using redis in cluster mode
* Corregge un bug nel sistema di calcolo di cambio stato in caso di attributi di tipo ezdate

**[opencontent/openpa_agenda-ls changes between 1.26.0 and 1.28.2](https://github.com/OpencontentCoop/openpa_agenda/compare/1.26.0...1.28.2)**
* Hotfix calendar height (workaround)
* Add bc interface if missing ocwebhookserver extension
* Corregge alcuni bug che producevano warning
* Incapsula le chiamate ai webhook trigger
* Introduce la visualizzazione delle sovrapposizioni degli eventi
* Corregge alcuni bug nella produzione del pdf
* Add newletter tools
* Rdesign del programma in pdf meno flessibile ma più dinamico
* Bugfix
* Corregge un bug nello stile di stampa del volantino

**[opencontent/openpa_bootstrapitalia-ls changes between f47dae6 and 6a641ce](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/f47dae6...6a641ce)**
* Indicizza i document extra con accesso anonimo
* Corregge un bug nel template della newsletter per cui i link nel footer non comparivano correttamente
* Utilizza il campo editor_access_in_footer dell'homepage (se presente) per maggiore flessibilità di configurazione al bottone di Accesso area personale
* Introduce le visualizzazioni per le classi lotto e dataset_lotto
* Corregge un problema nell'interfaccia di inserimento dei lotti
* Corregge una vulnerabilità xss
* Corregge un bug di consistenza in openparole
* Uniforma le etichette dei bottoni in content/edit
* Introduce un suffisso nel titolo delle card se presente e selezionato l'attributo is_online (bolzano)
* Permette di selezionare la tipologia di vista nel blocco contenuti remoti
* Hotfix configurazione di override
* Visualizza il suffisso al titolo nei banner
* Corregge alcuni errori di traduzione
* Evita un fatal error in editorialstuff se non è installata l'estensione ocevent
* Migliora la visualizzazione tabellare dei file multipli
* Corregge lo stile del suffisso al titolo
* Rende più flessibile la configurazione del blocco Ricerca documenti
* Corregge un errore nel template di visualizzazione degli attributi principali
* Introduce i template per la classe shared_link per virtualizzare contenuti remoti Visualizza la funzionalità webhooks in toolbar admin Corregge alcuni bug minori
* Cambia l'interfaccia di inserimento delle persone e delle strutture nel ruolo per aggirare il problema del numero elevato di elementi da caricare in select
* Corregge la traduzione dei giorni della settimana nella matrice degli orari
* Rimuove il testo della ricerca dal title per evitare un debug error
* Corregge la visualizzazione dei link nell'header
* Corregge la visualizzazione dei link nell'header
* Corregge la visualizzazione dei link nell'header (repetita iuvant)
* Nasconde il blocco contatti dal footer se i contatti non sono popolati
* Corregge alcuni bug minori
* Introduce la richiesta di conferma all'invio di una dashboard newsletter
* Corregge un bug di traduzione
* Estende l'indicizzazione custom rpt ai sotto attributi di tipo eZGmapLocation
* Aggiunge la configurazione mancante del modulo extraindex
* Rimuove eventuali script dai metadata opengraph
* Permette di customizzare la favicon tramite un attributo file della homepage (favicon)
* Corregge la visualizzazione degli eventi nella newsletter
* Corregge un errore sull'ordinamento delle faq
* Evita un errore grave nel parsing dei testi per opengraph
* Corregge un errore sull'ordinamento delle faq
* Corregge un problema di renderizzazione della mail prodotta dalla dashboard di moderazione
* Modifica la visualizzazione delle date usano lo standard locale Introduce un nuovo connettore per gli eztags per correggere la visualizzazione delle faccette Corregge alcune traduzioni in tedesco
* Impone il target blank ai contenuti shared link
* Corregge i font nel template della newsletter
* Rimuove la visualizzazione della data dai template newsletter
* Nasconde gli attributi info-collectors nel pagina di registrazione join
* Aggiunge l'attributo rel="noopener noreferrer" ai link con target blank
* Introduce una nuova versione del cookie consent
* Permette di disabilitare l'anteprima del video nelle visualizzazioni line e card (alt)
* Permette di configurare la versione del cookie consent da openpa.ini
* Introduce il bottone per rifiutare tutti i cookie nella barra dei cookie
* Corregge un problema nell'interfaccia di configurazione delle impostazioni di visualizzazione della vista elenco
* Introduce un blocco che permette la visualizzazione in anteprima di contenuti non accessibili
* Formatta un attributo matrice con identificatore "social" per inserire i contatti social (Facebook, Instagram, Linkedin, Twitter, Youtube, WhatsApp, Telegram)
* Visualizza nella pagina di login l'attributo layout di un oggetto il cui remote id è configurato in app.ini [LoginTemplate] LoginIntroObjectRemoteId
* Permette di inviare un messaggio privato dalla dashboard public/private in caso di gestione utenti
* Correggi alcuni problemi sul sistema cookie consent
* Correggi la visualizzazione mobile del sistema cookie consent
* Corregge alcuni bug minori
* Inserisce una stringa di traduzione mancante nel card-calendar
* Corregge l'etichetta dell'attributo Tipo di evento (has_public_event_typology)
* Corregge un errore di visualizzazione del calendario nella vista mensile
* Corregge alcuni problemi di accessibilità del cookie consent
* Introduce la vista card_teaser nei blocchi ListaManuale e ListaAutomatica
* Corregge un bug nella generazione del megamenu
* Corregge la traduzione del link per disiscriversi dalla newsletter
* Permette di verificare l'univocità del campo identificativo documento in fase di editing (document/has_code)
* Permette l'inserimento di file nella scorciatoia ajax di inserimento lotti
* Permette l'inserimento di file nella scorciatoia ajax di inserimento lotti
* Corregge un problema che si verificava nel blocco ricerca luoghi in caso di selezione di più tag
* Corregge alcuni problemi di overflow del testo nel card tesaser
* Aggiorna le stringhe di traduzione
* Corregge l'ingombro del cookie consent nel footer
* Permette di configurare il limiti dei link esposti in header
* Permette di configurare il controllo di univocità di altri campi di tipo stringa


## [2.1.0](https://gitlab.com/opencontent/openagenda/compare/2.0.10...2.1.0) - 2021-09-07
- Remove unused script
- Update docker image version Refactor configurations and tools
- Update README.md
- Fix minor bugs in event and private_organization
- Update nginx Docker tag to v1.18
- Fixed telephone number

#### Code dependencies
| Changes                               | From     | To          | Compare                                                                                    |
|---------------------------------------|----------|-------------|--------------------------------------------------------------------------------------------|
| asimlqt/php-google-spreadsheet-client | v3.x-dev |             |                                                                                            |
| aws/aws-crt-php                       |          | v1.0.2      |                                                                                            |
| aws/aws-sdk-php                       | 3.133.24 | 3.192.0     | [...](https://github.com/aws/aws-sdk-php/compare/3.133.24...3.192.0)                       |
| clue/stream-filter                    | v1.4.1   | v1.5.0      | [...](https://github.com/clue/stream-filter/compare/v1.4.1...v1.5.0)                       |
| composer/installers                   | 7d610d5  | 8669edf     | [...](https://github.com/composer/installers/compare/7d610d5...8669edf)                    |
| ezsystems/ezmultiupload-ls            | v5.3.1.2 | v5.3.1.3    | [...](https://github.com/ezsystems/ezmultiupload/compare/v5.3.1.2...v5.3.1.3)              |
| ezsystems/ezpublish-legacy            | 855cc50  | 2020.1000.6 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/855cc50...2020.1000.6)       |
| firebase/php-jwt                      |          | v5.4.0      |                                                                                            |
| friendsofsymfony/http-cache           | 7560f30  | a501495     | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/7560f30...a501495)          |
| google/apiclient                      |          | 3a98175     |                                                                                            |
| google/apiclient-services             |          | v0.211.0    |                                                                                            |
| google/auth                           |          | v1.18.0     |                                                                                            |
| guzzlehttp/promises                   | 89b1a76  | c1dd809     | [...](https://github.com/guzzle/promises/compare/89b1a76...c1dd809)                        |
| maxh/php-nominatim                    | 7bd5fd7  | 2.2.0       | [...](https://github.com/maxhelias/php-nominatim/compare/7bd5fd7...2.2.0)                  |
| monolog/monolog                       |          | f2156cd     |                                                                                            |
| mtdowling/jmespath.php                | 52168cb  | 9b87907     | [...](https://github.com/jmespath/jmespath.php/compare/52168cb...9b87907)                  |
| opencontent/ezpostgresqlcluster-ls    | 1c50dd6  | ef754ff     | [...](https://github.com/Opencontent/ezpostgresqlcluster/compare/1c50dd6...ef754ff)        |
| opencontent/googlesheet               |          | 63b7197     |                                                                                            |
| opencontent/ocbootstrap-ls            | 1.9.6    | 1.10.9      | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.6...1.10.9)               |
| opencontent/oceditorialstuff-ls       | 2.4.2    | 2.5.1       | [...](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.4.2...2.5.1)           |
| opencontent/ocembed-ls                | 1.4.1    | 1.4.2       | [...](https://github.com/OpencontentCoop/ocembed/compare/1.4.1...1.4.2)                    |
| opencontent/ocevents-ls               | 1.1.9    | 1.1.10      | [...](https://github.com/OpencontentCoop/ocevents/compare/1.1.9...1.1.10)                  |
| opencontent/ocfoshttpcache-ls         | aa42fb9  | 5ab0d91     | [...](https://github.com/OpencontentCoop/ocfoshttpcache/compare/aa42fb9...5ab0d91)         |
| opencontent/oci18n                    | 0f8551d  | c67c37e     | [...](https://github.com/OpencontentCoop/oci18n/compare/0f8551d...c67c37e)                 |
| opencontent/ocinstaller               | ce5b6e1  | 3a84656     | [...](https://github.com/OpencontentCoop/ocinstaller/compare/ce5b6e1...3a84656)            |
| opencontent/ocmultibinary-ls          | 2.2.1    | 2.3.1       | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.1...2.3.1)              |
| opencontent/ocopendata-ls             | 2.23.5   | 2.25.7      | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.5...2.25.7)               |
| opencontent/ocopendata_forms-ls       | 1.6.3    | 1.6.11      | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.3...1.6.11)          |
| opencontent/ocoperatorscollection-ls  | 2.1.1    | 2.2.0       | [...](https://github.com/OpencontentCoop/ocoperatorscollection/compare/2.1.1...2.2.0)      |
| opencontent/ocrecaptcha-ls            | 1.4      | 1.5         | [...](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.4...1.5)                    |
| opencontent/ocsearchtools-ls          | 1.10.9   | 1.11.1      | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.9...1.11.1)            |
| opencontent/ocsocialdesign-ls         | 1.6.2    | 1.6.3       | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.2...1.6.3)             |
| opencontent/ocsupport-ls              | 49a42aa  | 8071c61     | [...](https://github.com/OpencontentCoop/ocsupport/compare/49a42aa...8071c61)              |
| opencontent/ocwebhookserver-ls        | 32df3cf  | 069a0c3     | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/32df3cf...069a0c3)        |
| opencontent/openpa-ls                 | 20665ce  | 6272733     | [...](https://github.com/OpencontentCoop/openpa/compare/20665ce...6272733)                 |
| opencontent/openpa_agenda-ls          | 1.24.7   | 1.26.0      | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.7...1.26.0)            |
| opencontent/openpa_bootstrapitalia-ls | 98d712c  | f47dae6     | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/98d712c...f47dae6) |
| opencontent/openpa_theme_2014-ls      | 2.15.0   | 2.15.4      | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.0...2.15.4)        |
| paragonie/constant_time_encoding      |          | v2.4.0      |                                                                                            |
| paragonie/random_compat               |          | v9.99.100   |                                                                                            |
| php-http/client-common                | 03086a2  | 2.4.0       | [...](https://github.com/php-http/client-common/compare/03086a2...2.4.0)                   |
| php-http/discovery                    | 82dbef6  | 1.14.0      | [...](https://github.com/php-http/discovery/compare/82dbef6...1.14.0)                      |
| php-http/guzzle6-adapter              | bd7b405  | 9d1a45e     | [...](https://github.com/php-http/guzzle6-adapter/compare/bd7b405...9d1a45e)               |
| php-http/httplug                      | c9b2424  | 191a0a1     | [...](https://github.com/php-http/httplug/compare/c9b2424...191a0a1)                       |
| php-http/message                      | fcf9b45  | 1.12.0      | [...](https://github.com/php-http/message/compare/fcf9b45...1.12.0)                        |
| php-http/promise                      | 02ee67f  | 4c4c1f9     | [...](https://github.com/php-http/promise/compare/02ee67f...4c4c1f9)                       |
| phpseclib/phpseclib                   |          | 3.0.x-dev   |                                                                                            |
| psr/cache                             |          | 1.0.1       |                                                                                            |
| psr/event-dispatcher                  | cfea31e  | aa4f89e     | [...](https://github.com/php-fig/event-dispatcher/compare/cfea31e...aa4f89e)               |
| psr/http-client                       | fd5d37a  | 22b2ef5     | [...](https://github.com/php-fig/http-client/compare/fd5d37a...22b2ef5)                    |
| psr/http-factory                      |          | 36fa03d     |                                                                                            |
| psr/log                               | e1cb6ec  | 1.1.4       | [...](https://github.com/php-fig/log/compare/e1cb6ec...1.1.4)                              |
| symfony/deprecation-contracts         | ede224d  | 6f981ee     | [...](https://github.com/symfony/deprecation-contracts/compare/ede224d...6f981ee)          |
| symfony/event-dispatcher              | 7d4f83e  | 5.4.x-dev   | [...](https://github.com/symfony/event-dispatcher/compare/7d4f83e...5.4.x-dev)             |
| symfony/event-dispatcher-contracts    | 5a749bd  | 2.5.x-dev   | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5a749bd...2.5.x-dev)   |
| symfony/options-resolver              | 1942f69  | 5.4.x-dev   | [...](https://github.com/symfony/options-resolver/compare/1942f69...5.4.x-dev)             |
| symfony/polyfill-ctype                | 4719fa9  | 46cd957     | [...](https://github.com/symfony/polyfill-ctype/compare/4719fa9...46cd957)                 |
| symfony/polyfill-intl-idn             |          | 65bd267     |                                                                                            |
| symfony/polyfill-intl-normalizer      |          | 8590a5f     |                                                                                            |
| symfony/polyfill-mbstring             | 766ee47  | 9174a3d     | [...](https://github.com/symfony/polyfill-mbstring/compare/766ee47...9174a3d)              |
| symfony/polyfill-php72                |          | 9a14221     |                                                                                            |
| symfony/polyfill-php73                | 0f27e9f  | fba8933     | [...](https://github.com/symfony/polyfill-php73/compare/0f27e9f...fba8933)                 |
| symfony/polyfill-php80                |          | 1100343     |                                                                                            |
| symfony/yaml                          | 0c7023a  | 5f947e5     | [...](https://github.com/symfony/yaml/compare/0c7023a...5f947e5)                           |
| zetacomponents/base                   | 1.9.1    | 1.9.3       | [...](https://github.com/zetacomponents/Base/compare/1.9.1...1.9.3)                        |
| zetacomponents/console-tools          | 1.7      | 1.7.2       | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7...1.7.2)                  |
| zetacomponents/mail                   | 1.9.1    | 1.9.2       | [...](https://github.com/zetacomponents/Mail/compare/1.9.1...1.9.2)                        |
| zetacomponents/persistent-object      | 1.8      | 1.8.1       | [...](https://github.com/zetacomponents/PersistentObject/compare/1.8...1.8.1)              |


Relevant changes by repository:

**[opencontent/ezpostgresqlcluster-ls changes between 1c50dd6 and ef754ff](https://github.com/Opencontent/ezpostgresqlcluster/compare/1c50dd6...ef754ff)**
* Check OpenPABase class exists
* Add static method storeClusterizedFileMetadata for migrations

**[opencontent/ocbootstrap-ls changes between 1.9.6 and 1.10.9](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.6...1.10.9)**
* Disabilita il drag and drop nell'edit di un attributo ezpage
* Evita in warning in siteaccess di backend per override mancante
* Corregge un bug che impediva la corretta visualizzazione dei checkbox nei form dinamici
* Corregge un errore nella pagina di conferma pubblicazione
* Introduce i template bootstrap 4 per ezsurvey
* Corregge la visualizzazione dei form per ezsurvey in bootstrap 4
* Corregge un bug che impediva la visualizzazione dei testi di aiuto nei form dinamici
* Corregge una vulnerabilità XSS
* Aggiorna le traduzioni delle stringhe
* Protegge con CSRF token la chiamata post ajax di ocevents
* Protegge con CSRF token la chiamata post ajax di ocmultibinary
* Aggiorna la stringhe di traduzione
* Aggiorna la stringhe di traduzione
* Introduce un workaround per evitare che Chrome auto-popoli il campo ezuser
* Aggiorna le stringhe di traduzione

**[opencontent/oceditorialstuff-ls changes between 2.4.2 and 2.5.1](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.4.2...2.5.1)**
* Permette la personalizzazione delle notifiche mail (per le azioni  OCEditorialStuffActionHandler::notifyOwner e  OCEditorialStuffActionHandler::notifyGroup)
* Visualizza la traduzione inserita in cronologia versioni

**[opencontent/ocembed-ls changes between 1.4.1 and 1.4.2](https://github.com/OpencontentCoop/ocembed/compare/1.4.1...1.4.2)**
* Evita la richiesta justcheckurl per workaround a problema youtube

**[opencontent/ocevents-ls changes between 1.1.9 and 1.1.10](https://github.com/OpencontentCoop/ocevents/compare/1.1.9...1.1.10)**
* Inserisce il token csrf nella chiamata post ajax

**[opencontent/ocfoshttpcache-ls changes between aa42fb9 and 5ab0d91](https://github.com/OpencontentCoop/ocfoshttpcache/compare/aa42fb9...5ab0d91)**
* Permette di inserire l'id utente nel context hash
* Minor bugfix

**[opencontent/oci18n changes between 0f8551d and c67c37e](https://github.com/OpencontentCoop/oci18n/compare/0f8551d...c67c37e)**
* Add classes and tag csv import export tools
* Fix language parser in update installer
* Update google api

**[opencontent/ocinstaller changes between ce5b6e1 and 3a84656](https://github.com/OpencontentCoop/ocinstaller/compare/ce5b6e1...3a84656)**
* Add installer type (for submodules)
* Show create/update tag in debug message
* Add sort and priority in content steps Fix var replacer Add sql_copy_from_tsv type Fix bugs
* Bugfixes
* Allow content update Assign role by remote
* Fix dump table
* Accept custom date() var
* Add remove role assignment Add version compare condition
* Add classid var function calling eZContentClass::classIDByIdentifier
* add classattributeid and classattributeid_list expression function
* Check trash Allow extra root datadir path
* Add --force option to ignore version check Add add_tag, remove_tag, move_tag to handle single tag modification
* Add patch_content to update single content attribute
* Fix compatibility with eZ version 5.90.0
* Add sort data in patch_content Fix patch_content error handling Improve version_compare Fix bug in tagtree synonyms Add dump script tools
* Write installer log to file
* Fix workflow installer
* Fix expiryPassword in dryrun
* Add change_state, change_section, tag_description Bugifx
* Add reindex step
* Add remove extra locations feature in content tree installer
* Now inside a resource can import another resource
* Make bigint type to ezcontentobject.published and ezcontentobject.modified
* add install pgcrypto script
* Fix dump_tag_tree tool, add recaptcha3 installer
* Minor fixes
* Add deprecate_topic installer step
* Allow deprecate topic without replacement
* Avoid error in DeprecateTopic installer
* Fix pagination in DepracateTopic
* Move node before remove location avoiding block inconsistency
* Add rename_tag step
* Fix rename_tag step
* Add load_policies param in role step (se false to avoid role regeneration)
* avoid warning in IOTools
* Fix patch content attribute parser

**[opencontent/ocmultibinary-ls changes between 2.2.1 and 2.3.1](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.1...2.3.1)**
* Permette il download se l'oggetto è in draft e l'utente corrente ne è il proprietario (per preview immagini)
* Add missing translation
* Add error translations
* Add ini setting to configure allowed file extension per attribute
* Allow to clear all files in fromString method
* Add CSRF token in ajax post

**[opencontent/ocopendata-ls changes between 2.23.5 and 2.25.7](https://github.com/OpencontentCoop/ocopendata/compare/2.23.5...2.25.7)**
* Ottimizza la creazione di sinonimi e di traduzioni di tag via api
* Aggiunge il remote id del main node all'Api Content Metadata
* Permette la creazione e la modifica via api di attributi di tipo ezpage
* Corregge un problema nel calcolo del nodo principale in Metadata
* Merge branch 'ezpage_rest_field'    ezpage_rest_field:   Permette la creazione e la modifica via api di attributi di tipo ezpage
* Corregge lo svuotamento cache del Section repository
* Corregge l'esposizione di oggetti correlati in default environment qualora essi siano convertiti in formato esteso
* Corregge un bug sul parsing dei tag nel AttributeConverter
* Aumento il limite massimo di ricerca in env default a 300 (per compatibilità con ocopendata_forms)
* Corregge un bug che si verificava nella pubblicazione di contenuti in installazioni multilingua
* Mantiene la stessa dimensione dell'array di risposta nelle richieste select-fields anche se manca la traduzione nella lingua corrente
* opendataTools i18n visualizza la prima traduzione esistente qualora non esistesse nella lingua corrente né nella lingua di fallback
* opendataTools corregge un bug sul metodo i18n
* Permette di bypassare le policy di creazione/aggiornamento in api content repository
* Corregge un bug nella validazione api delle relazioni
* Permette di specificate le preferenze di lingua in TagRepository
* Evita un possibile errore nel calcolo della paginazione a cursore
* Force la rigenerazione delle cache delle classi per evitare un fatal error
* Permette di definire una blacklist dei metadati esposti agli utenti anonimi
* Evita di ridondare le virgolette nelle parser delle query in formato stringa
* Corregge il percorso di download nella conversione a ckan della risorsa di tipo eZBinaryFile
* Corregge una potenziale vulnerabilità nel repository delle api

**[opencontent/ocopendata_forms-ls changes between 1.6.3 and 1.6.11](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.3...1.6.11)**
* Espone l'indirizzo da geocoder nominatim in modo meno verboso
* Permette l'utilizzo della libreria UploadFieldConnector in esecuzioni da linea di comando
* Rimuove un log di debugging
* Corregge un problema di configurazione
* Permette di tradurre di un contenuto qualora si tenti di modificarlo in una lingua non ancora presente
* Impedisce l'invio del form alla pressione del tasto invio solo nell'ambito di alpaca-form
* Corregge alcuni bug minori
* Corregge un bug nella ricerca del punto sulla mappa nel contesto di un form dinamico
* Corregge il valore di default del campo BooleanField in accordo con i parametri dell'attributo di classe

**[opencontent/ocoperatorscollection-ls changes between 2.1.1 and 2.2.0](https://github.com/OpencontentCoop/ocoperatorscollection/compare/2.1.1...2.2.0)**
* Avoid switch to user with admin capabilities

**[opencontent/ocrecaptcha-ls changes between 1.4 and 1.5](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.4...1.5)**
* Verifica la presenza del valore post in validateObjectAttributeHTTPInput
* Verifica la presenza del valore post in validateCollectionAttributeHTTPInput
* Disabilita il connettore recaptcha se l'utente corrente è autenticato

**[opencontent/ocsearchtools-ls changes between 1.10.9 and 1.11.1](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.9...1.11.1)**
* Rimuove di default le cache calendartaxonomy e calendarquery che vanno attivate qualora di utilizzino le funzionalità
* Evita un errore nella sincronizzazione dei gruppi di classe

**[opencontent/ocsocialdesign-ls changes between 1.6.2 and 1.6.3](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.2...1.6.3)**
* Permette l'inserimento di link nel testo dei credits

**[opencontent/ocsupport-ls changes between 49a42aa and 8071c61](https://github.com/OpencontentCoop/ocsupport/compare/49a42aa...8071c61)**
* Add installer info
* Add changelog tools
* changelog bugfixes
* Fix tag listing tool
* Bugifx in tag list

**[opencontent/ocwebhookserver-ls changes between 32df3cf and 069a0c3](https://github.com/OpencontentCoop/ocwebhookserver/compare/32df3cf...069a0c3)**
* Add simple endpoint api
* Add class definition shared_link
* Fix trigger check, add response to test
* Allow payload customizations via trigger configuration
* Fix custom payload check in webhook edit
* Add retry system
* Bugfix sql update
* Make pusher request timeout configurable by ini
* Fix retry calculation
* Add single job view
* Fix cleanup on maximum number of attempts reached
* Add bootstrapitalia design

**[opencontent/openpa-ls changes between 20665ce and 6272733](https://github.com/OpencontentCoop/openpa/compare/20665ce...6272733)**
* Permette l'accesso a openpa/roles a gui custom (bootstrapitalia)
* Migliora lo script di creazione di una nuova istanza
* Corregge alcuni potenziali problemi di sicurezza xss
* Evoluzione di robots.txt per bloccare i bad crawler
* Coregge un problema nella creazione dell'oranigramma  2874
* Considera content_tag_menu nel calcolo del tree menu
* In openpa/object viene considerato il main node
* In openpa/object viene considerato il main node
* Aggiunge WhatsApp e Telegram tra i valori possibili dei contatti
* Aggiunge il codice SDI ai contatti
* Aggiunge il codice SDI ai contatti
* Aggiunge la possibilità di escludere alberature dai menu
* Aggiunge la possibilità di escludere alberature dai menu
* Permette la configurazione del mittente email da applicazione
* Rimuovo ruolo e ruolo2 da ContentMain/AbstractIdentifiers  1940
* Rimuovo ruolo e ruolo2 da ContentMain/AbstractIdentifiers  1940
* Considera la variabile d'ambiente EZ_INSTANCE per la definizione dell'identificativo dell'istanza corrente
* Carica la versione del software in CreditsSettings::CodeVersion da file VERSION se presente
* Corregge lo script di migrazione da nfs a s3
* Rimuove codice inutilizzato o obsoleto
* Nella configurazione cluster la cache di ocopendata viene salvata in locale invece che in redis
* Aggiunge TikTok ai possibili contatti
* Considera la variabile d'ambiente EZ_INSTANCE per la definizione dell'identificativo del siteaccess custom per evitare conflitti con i nomi dei moduli
* Aggiunge il link all'area personale ai valori inseribili nei contatti
* Aggiunge TikTok ai possibili contatti
* Aggiunge il link all'area personale ai valori inseribili nei contatti
* Corregge un errore nella funzione di template per la ricerca delle strutture
* Corregge un errore nella funzione di template per la ricerca delle strutture
* Corregge un bug in OpenPASMTPTransport
* Corregge il riconoscimento automatico del software in uso per la visualizzazione delle metriche di utilizzo
* Corregge un bug nella definizione degli handler dei blocchi
* Corregge un bug nella definizione degli handler dei blocchi
* Permette la personalizzazione dei valori head/meta da openpa/seo
* Permette la personalizzazione dei valori head/meta da openpa/seo
* Visualizza la versione dell'installer se presente in CreditSettings/CodeVersion
* Visualizza la versione dell'installer se presente in CreditSettings/CodeVersion
* Corregge un bug in openpa/object
* Corregge un bug in openpa/object
* Corregge la fetch per il controllo degli stati
* Corregge la fetch per il controllo degli stati
* Permette a content_link di riconoscere se un link è al nodo o a un nodo/link esterno
* Permette a content_link di riconoscere se un link è al nodo o a un nodo/link esterno
* Corregge un possibile errore nel calcolo delle aree tematiche
* Corregge un possibile errore nel calcolo delle aree tematiche
* Corregge un typo in cookie
* Corregge un typo in cookie
* Migliora la visualizzazione della versione del software
* Migliora la visualizzazione della versione del software
* Corregge l'id del tree menu in caso di menu virtualizzato su alberatura di tag
* Corregge l'id del tree menu in caso di menu virtualizzato su alberatura di tag
* Permette la configurazione di Google Recaptcha v3
* Permette la configurazione di Google Recaptcha v3
* Aggiorna le configurazioni degli attributi di classe direttamente dal openpa/recaptcha
* Aggiorna le configurazioni degli attributi di classe direttamente dal openpa/recaptcha
* Permette l'inserimento del codice Web Analytics Italia
* Permette l'inserimento del codice Web Analytics Italia
* Aggiunge il protocollo (https per default) in aws dfs public handler
* Fixed key definition name in openpaini
* Add redis cluster connector verbose logs
* Permette di gestire tramite permessi il menu trasparenza
* Merge branch 'master' into version3   Conflicts:  	bin/php/create_instance.php  	classes/openpaini.php  	design/standard/templates/openpa/seo.tpl
* Corregge un bug nel modulo usage/metrics
* Corregge un possibile errore nel salvataggio della matrice dei contatti (e introduce uno script per il fix del pregresso)
* Merge branch 'master' into version3 Corregge un possibile errore nel salvataggio della matrice dei contatti (e introduce uno script per il fix del pregresso)
* Ordina le serie nel grafico delle partecipazioni
* Merge branch 'master' into version3
* Considera l'effettiva capacità di lettura dell'utente degli oggetti correlati nella definizione di hasContent per attributi di tipo eZObjectRelationListType
* Corregge alcuni bug minori
* Nella configurazione dispatcher cluster: sostituisce l'handler local con s3_private_cache e rende i bucket configurabili separatamente
* Corregge la lingua di destinazione nel modulo per la copia dell'oggetto (openpa/add)
* Corregge una vulnerabilità xss nel nome utente
* Merge branch 'master' into version3
* Corregge le regole di default del robots.txt per permettere l'indicizzazione dei contenuti opendata

**[opencontent/openpa_agenda-ls changes between 1.24.7 and 1.26.0](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.7...1.26.0)**
* Introduce la pagina statistiche
* Merge branch 'master' into feature_stat    master:   Aggiorna le traduzioni in greco   Aggiorna le traduzioni in greco   Corregge un bug di renderizzazione delle date e alcuni problemi di traduzione   Genera il pdf con wkhtmltopdf se presente in env   Espone il link corretto ai cookie configurati in agenda root
* Corregge un problema di visualizzazione della dashboard dei programmi in pdf
* Corregge la visualizzazione dei link social
* Aggiunge il bottone di ricerca e corregge la visualizzazione dei blocchi home
* Introduce uno script per rendere tutte le organizzazioni private
* Introduce l'integrazione con ocwebhookserver

**[opencontent/openpa_bootstrapitalia-ls changes between 98d712c and f47dae6](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/98d712c...f47dae6)**
* Corregge un bug sulla visualizzazione di un link come card
* Corregge l'icona di modifica dell'icona per l'accesso alla modifica del link/banner
* Corregge la visualizzazione dell'attributo external_contact_point
* Evita un possibile errore nell'edit handler
* Corregge la traduzione di alcune stringhe statiche
* Corregge la traduzione di alcune stringhe statiche
* Introduce ezsurvey in websitetoolbar
* Permette durante la redazione di un articolo di creare documenti nella cartella "Documenti (tecnici) di supporto" ma di poter navigare e allegare Documenti che stanno anche in altre sezioni
* Permettere di nascondere il link di accesso all'area personale (se presente un attributo booleano 'hide_access_menu' in homepage)
* Migliora la modalità selezione/inserimento dei luoghi in un evento
* Corregge una regressione riapplicando il font serif ai blocchi di testo nelle visualizzazioni full
* Espone gli attributi nell'ordine corretto in visualizzazione full
* Corregge l'esposizione degli attributi nell'ordine corretto in visualizzazione full considerando anche i custom attributes
* Formatta le card degli oggetti di classe html_content visualizzando solo il contenuto non filtrato dell'attributo html
* Migliora il blocco per l'inclusione di contenuti remoti aggiungendo facets e impostazioni avanzate
* Corregge un possibile errore nel attributo di tipo reverse relations
* Merge branch 'improve-remote-block'
* Corregge un errore per cui non era possibile modificare un banner se il link era interno
* Considera le date di inizio e fine e migliora la visualizzazione dei ruoli openpa
* Permette l'inserimento del link Vedi tutti nel blocco Contenuti remoti
* Permette l'edit inline della descrizione del tag
* Permettere di attivare il menu esteso (se presente un attributo booleano 'show_extended_menu' in homepage)
* Corregge un problema di visualizzazione della descrizione del tag
* Corregge un problema di visualizzazione nelle select chosen
* Permette di visualizzare attributi in formato information collector
* Revert: Corregge un problema di visualizzazione nelle select chosen
* Corregge il colore del testo del banner se nessun colore di sfondo è selezionato
* Permette una maggiore customizzazione del blocco Contenuti remoti
* Corregge un errore di rendering javascript nel blocco lista paginata
* Corregge l'ordinamento dei contenuti extra nella pagina trasparenza
* Migliora il form dinamico di inserimento e modifica del Documento inserendo e validando gli attributi file e link
* Migliora l'ordinamento delle persone nell'attributo di tipo ruolo
* Abilita la possibilità di navigare in tutta l'alberatura per tutti gli attributi di tipo relazioni oggetti con vista "sfoglia"
* Permette di di modificare testo del cookie alert
* Corregge alcune imprecisioni sull'utilizzo dei font
* Corregge l'ordinamento dei ruoli per priorità
* Nasconde il titolo nei blocchi relazionati (con vista card teaser) nel caso di un unico oggetto relazionato per gli attributi has_spatial_coverage, has_online_contact_point, opening_hours_specification
* Imposta l'immagine originale per i blocchi ricerca e argomenti per evitare la degenerazione dell'immagine
* Nasconde il bullet dall'elenco ruoli in caso di ruolo singolo
* Corregge alcune stringhe di traduzione
* Migliora la paginazione del blocco lista_paginata
* Migliora la visualizzazione del pulsante canale digitale
* Permette l'utilizzo di un'alberatura di argomenti invece che di una lista piatta
* Corregge un problema per cui non veniva visualizzata l'obbligatorietà dei campi relazioni nei form dinamici
* Corregge un problema per cui non veniva correttamente visualizzata l'icona nei risultati di ricerca
* Aggiornamento delle dipendenze javascript Introduzione del nuovo tema warmred
* Corregge un bug nel blocco Contenuti remoti
* Imposta lo sfondo grigio nel primo blocco di default nella pagina argomento
* Imposta di default lo sfondo esteso al primo blocco inserito da editor negli argomenti
* Permette di specificare il formato responsive per le tabelle
* Gestisce gli argomenti presenti nell'alberatura custom degli argomenti (custom_topics) negli input di ricerca
* Espone i menu virtualizzati su alberatura di tag nei motori di ricerca
* Permette di filtrare solo i campi obbligatori nel form di creazione o modifica di un contenuto
* Aggiorna le stringhe di traduzione
* Rende disponibile l'interfaccia di inserimento luoghi anche per classi diverse da event
* Formatta la tabella dei dati dei grafici
* Corregge il limite di default per i sotto argomenti
* Visualizza document/description nel blocco ricerca_documenti
* Imposta le zone e i blocchi per pagina_trasparenza
* Imposta come predefinita la visualizzazione a mappa nel blocco ricerca_luoghi
* Permette di escludere tipi di contenuto nel blocco argomenti
* Visualizza le lingue disponibili per il contenuto solo se non è presente il language switcher
* Corregge un errore di digitazione
* Imposta le categorie di attributo di default in content.ini
* Corregge un problema di visualizzazione della pagina se non è presente il menu laterale
* Migliora la visualizzazione del pannello strumenti in funzione delle dahsboard collaborative
* Corregge un problema del calendario per cui non veniva visualizzata correttamente la data di fine evento
* Permette di modificare la singola traduzione dal pannello Info
* Predispone un nome di attributo per gestire il redirect al login
* Corregge il breadcrumb delle pagine argomento
* Aggiunge il controllo dello stato Privacy nelle openapi
* Introduce un modulo per raccogliere dagli utenti riscontri sull’utilizzo del sito
* Migliora la visualizzazione del blocco Lista Paginata utilizzando le viste presenti negli altri blocchi
* Permette di aggiungere un'immagine di sfondo nel blocco remote contents
* Pemette di aggiungere una breve descrizione nei blocchi lista automatica, manuale e paginata
* Corregge un problema di visualizzazione nella vista di conferma pubblicazione
* Visualizza icona e tipo di contenuto nei risultati del motore di ricerca
* Corregge un problema di visualizzazione delle schede contatti che si verificava in Chrome
* Imposta l'internazionalizzazione delle stringhe del motore di ricerca
* Permette di nascondere la Guida al cittadino delle pagine trasparenza tramite configurazione
* Migliora l'usabilità del motore di ricerca introducendo i parametri di ordinamento
* Visualizza ufficio e area nella pagina argomento
* Abilita il layout di default per la classe politico
* Impedisce che gli indirizzi email vadano a capo nelle card teaser
* Aggiunge il link al titolo e all'immagine delle card
* Aggiunge il modulo feedback nella pagina di ricerca
* Allarga lo spazio del bottone read more nel blocco oggetto singolo
* Permette di visualizzare la tagline anche in caso di opzione solo logo
* Corregge alcuni bug minori
* Corregge un bug di calcolo dei colori nel banner
* Rimuove dai template gli stili inline
* Ridisegna il blocco ricerca luoghi
* Corregge la visualizzazione delle pagine tags/view
* Permette di inserire tramite configurazione un testo introduttivo al pannello Gestione collaborativa dei contenuti
* Abilita chosen su mobile
* Visualizza il testo introduttivo nel blocco Lista Paginata
* Corregge alcuni bug minori
* Corregge alcuni bug nel rendering della valutazione
* Evidenza maggiormente il titolo nei risultati di ricerca
* Corregge un bug nella configurazione base di editorialstuff
* Corregge la configurazione dei generatori di dataset
* Corregge il meccanismo di moderazione per cui tutti i contenuti previsti dalle dashboard venivano sempre messi in moderazione
* Corregge la visualizzazione delle date in apertura nelle card
* Corregge un problema di digitazione
* Corregge la dimensione del testo del contenuto introduttivo della dashboard di collaborazione
* Corregge alcuni problemi che si verificavano in un'installazione multilingua
* Corregge la visualizzazione banner color di default
* Rimuove il bordo scuro dal box card_teaser
* Visualizza l'assessore di rifermento in topic
* Corregge un bug sulla ricerca ruoli negli attributi di tipo Ruolo
* Aumenta il ranking dei contenuti di tipo pagina_sito e frontpage
* Corregge un bug sul filtro di visualizzazione dei ruoli
* Invia una notifica all'utente o al gruppo configurato in caso di registrazione dal modulo join
* Permette di rimuovere/editare un link dentro una pagina trasparenza
* Aggiunge un nuovo tema per apss
* Nel servizio pubblico, nasconde il titolo del canale digitale se ne è correlato solo uno
* Corregge la visualizzazione della dashboard Gestione contenuti e imposta la dashboard di default per i luoghi
* Visualizza il link in channel come bottone
* Migliora la visualizzazione del blocco ricerca documenti
* Rende indicizzabili gli attributi di tipo Ruolo OpenPA per gli oggetti riferiti all’attributo person
* Reindicizza le persone quando ne si aggiornano i ruoli
* Permette il tracciamento da parte di webanalytics.italia.it
* [hotfix] Corregge un bug javascript nel blocco ricerca_documenti
* Corregge un problema che si potrebbe verificare nella reindicizzazione delle persone in creazione/aggiornamento ruoli
* Corregge un bug di visualizzazione nel blocco Ricerca documenti
* Introduce un nuovo datatype per poter decorare automaticamente una stringa
* aggiunto nuovo tema Mediterraneo
* aggiunto nuovo tema Mediterraneo
* Inverte l'ordine degli attributi managed_by  nella visualizzazione full del topic
* Aggiunge la categoria di attributo Concorsi
* Corregge un bug sulla visualizzazione di default del topic
* Disattiva di default la sincronizzazione della trasparenza
* Redesign del blocco singolo (conforme a template bootstrapitalia)
* Rimuove il nome file dalla visualizzazione di ezbinaryfile
* Migliora il blocco ricerca_documento introducendo il filtro per argomenti
* Semplifica le regole di visualizzazione delle pagina trasparenza
* Permette di ricercare in tutto il sito i contenuti da relazionare nell'interfaccia ajax
* Evidenza gli argomenti che hanno elementi figli nell'interfaccia di edit
* Migliora la visualizzazione dei ruoli
* Corregge alcuni bug minori
* Corregge un problema di visualizzazione in ricerca documenti
* Corregge un bug nella pagina trasparenza
* Corregge un errore grave nella logica di visualizzazione della pagina trasparenza
* Corregge una vulnerabilità XSS
* Corregge un bug nella paginazione della dashboard Segnalazioni degli utenti
* Introduce la paginazione per le Persone che compongono la struttura Corregge alcuni bug di configurazione
* Permette di escludere l'immagine principale dalla visualizzazione full
* Corregge un bug per cui non veniva visualizzata l'immagine principale nella galleria in caso di opzione attivata Mostra solo galleria
* Aggiunge l'indice extra geo per office public_organization public_service administrative_area
* Permette la ricerca con gli apici doppi nel blocco contenuti remoti
* Corregge un problema del tema mediterraneo
* Introduce la funzionalità delle dashboard remote (beta)
* Corregge un bug al connettore remote dashboard import
* Permette di rendere il menu laterale navigabile (da configurazione ini)
* Corregge un bug di visualizzazione di Chrome nell'immagine del blocco singolo di default
* Corregge il blocco contenuti remoti nel caso si visualizzino contenuti locali con un prefisso di siteaccess
* Migliora l'usabilità del selettore delle lingue
* Permette di nascondere l'etichetta di un gruppo di attributi
* Merge branch 'feature-lang-switcher'
* Aumenta l'abstract visualizzato in banner a 160 caratteri
* Permette di eseguire una ricerca nel contesto content/browse
* Considera la tipologia di configurazione di accesso host_uri nel selettore delle lingue
* Mostra gli argomenti figli sotto alla descrizione dell'argomento (controllato da show_topic_children) Mostra la descrizione dell'argomento a tutto ingombro se non sono presenti aree o organi politici relazionati Controlla la visibilità degli argomenti in caso di argomenti non accessibili all'anonimo Imposta il boost del topic nel motore di ricerca
* Nasconde gli argomenti deprecati dal menu
* Nasconde gli argomenti deprecati dal blocco argomenti
* Considera anche il campo Documento allegati come possibile elemento obbligatorio della classe Documento, oltre a Link e File
* Corregge un problema sulla validazione degli allegati nel documento
* Abilita le traduzioni nelle dashboard
* Permette di personalizzare la paginazione dei membri di un organo politico Permette di personalizzare il nome di un ruolo
* Imposta la configurazione di override dei template per visualizzare una matrice come elenco di link
* Corregge una vulnerabilità xss nella pagina di ricerca
* Corregge una vulnerabilità di tipo user enumeration nel modulo di recupero password
* Premette l'abilitazione/disabilitazione di un utente da interfaccia di frontend
* Corregge un bug nel modulo user/setting
* Introduce una vista datatable (in versione beta) per il blocco contenuti remoti
* Corregge alcuni problemi di accessibilità WCAG 2.1 AA
* Merge branch 'fix-wave'
* Migliora l'utilizzo dell'interfaccia in modalità multilingua
* Introduce un'interfaccia per la gestione dei tipi di ruolo (beta)
* Visualizza le traduzioni disponibili nell'interfaccia di modifica relazioni
* Migliora la visualizzazione del menu trasparenza
* Consente di specificare i ruoli da visualizzare in contenuti con attributi openparole
* wip dashboard mail
* Migliora il blocco ricerca documenti introducendo il filtro per anno, la possibilità di nascondere la data di fine pubblicazione e la possibilità di mostrare solo i contenuti in pubblicazione
* Corregge un bug sul blocco calendario per cui non veniva considerato il filtro per argomento
* Corregge la visualizzazione datatable del blocco contenuti remoti
* Corregge un problema di invio nel connettore della dashboard con una mailing list
* Corregge la visualizzazione dei giorni di chiusura negli orari
* Migliora la visualizzazione delle aree riservate
* Typo fix
* Modifica il link con ancora  page-content
* Migliora la visualizzazione delle dashboard Consente l'invio di mailing list per lingua
* Inserisce la traduzione mancante di "Leggi di più"
* Corregge un bug per cui le tipologie di contatto nei punti di contatto non erano correttamente modificabili
* Corregge un bug per cui  i contenuti nei blocchi non venivano filtrati correttamente per stato
* Visualizza i tipi di contatto disponibili a partire dal vocabolario dedicato
* Introduce il gruppo di attributi Bandi/Progetti
* Introduce un cron per eseguire le importazioni forzando il token di protezione
* Corregge un bug di traduzione nel template public service
* Corregge un bug nel blocco contenuti remoti
* Introduce un permesso custom (advanced_editor_tools) per la gestione dei permessi di accesso a funzionalità di editing avanzato
* Aggiorna la stringhe di traduzione
* Corregge un bug nella paginazione della visualizzazione dei contenuti per tag
* Rimuove il nome del file originale in ocmultibinary
* Corregge un bug di visulizzazione della lista paginata
* Corregge un bug nella ricerca filtrata dei contenuti remoti
* Recepisce la blacklist dei metadati esposti agli utenti anonimi Permette di nascondere i credits tramite configurazione ini
* Corregge la visualizzazione del logo nel footer in caso di solo logo e tagline
* Permette di virtualizzare il menu su un'alberatura di tag a tutti i livelli
* Hotfix themes!
* Corregge un bug sulla selezione dei sinonimi dei ruoli in openparole
* Corregge una vulnerabilità XSS sulla pagina di ricerca
* Corregge un id duplicato in website toolbar
* Corregge un bug per cui la dashboard di moderazione privacy non imponeva lo stato corretto
* Introduce un workaround per invalidare la cache della pagina Aree riservate
* Corregge la visualizzazione mobile dei bottoni dell'header
* Nel motore di ricerca di sezione preseleziona il filtro sulla pagina corrente se presente nei filtri di ricerca
* Permette di selezionare un'alberatura di tag dalla configurazione dei blocchi
* Visualizza il limite massimo di caricamento dei file
* Corregge la visualizzazione dei filtri nel blocco contenuti remoti
* Migliora l'interfaccia di modifica dei blocchi
* Corregge la pagina di redirect dopo la creazione/modifica di un contenuto
* Corregge un bug sulla ricerca nel blocco contenuti remoti
* Introduce un sistema di visualizzazione di faq
* Corregge alcuni bug minori
* Permette la creazione/modifica di faq inline
* Aggiorna le stringhe di traduzione
* Nasconde l'icona che permette di espandere il menu una volta espanso
* Permette di configurare le varianti del tema bi per la testata
* Corregge un bug di visualizzazione dell'icona menu
* Permette di filtrare la lista dei ruoli politico/amministrativi per ruoli scaduti
* Visualizza la toolbar in tutti i moduli
* Corregge un problema di visualizzazione dei filtri nella pagina di ricerca
* Customizza la visualizzazione della card-image per la classe grafico e dataset Migliora l'interfaccia di edit del grafico
* Corregge un errore di configurazione dei blocchi
* Corregge un problema di visualizzazione dei blocchi nelle pagine trasparenza
* Aggiunge un bordo alla toolbar
* Aumenta il limite nella ricerca dei luoghi in edit dell'evento
* Corregge un bug sulla visualizzazione della sezione di faq
* Permette di personalizzare l'etichetta Informazioni del footer e di specificare molteplici link sul contatto web
* Corregge alcuni problemi di validazione wcag in wave
* Aggiunge alcune traduzioni mancanti della pagina trasparenza
* Visualizza il menu utente per l'utente loggato anche quando è disabilitata la visualizzazione del bottone di accesso
* Fix typo
* Permette di controllare la visualizzazione del menu trasparenza dall'oggetto trasparenza
* Corregge alcuni bug minori Permette la configurazione di campi obbligatori nella classe valutazione Consente di utilizzare il blocco html in modalità wide Introduce un api per aggiungere contenuti esterni nei risultati del motore di ricerca (beta) Introduce una nuova vista Immagine decorativa per il blocco singolo Consente di esportare in csv l'elenco delle valutazioni inserite dagli utenti Consente di attivare o disattivare tutti i permessi di accesso per ciascun utente Visualizza il punto di contatto del box di aiuto nella pagina argomento Permette di aggiungere un testo introduttivo in tutti i blocchi Rimuove il numero tra parentesi dal nome visualizzato
* Corregge il colore dei caratteri dell'header nel tema warmred (bolzano)
* Migliora la visualizzazione dell'attributo public_service/has_temporal_coverage

**[opencontent/openpa_theme_2014-ls changes between 2.15.0 and 2.15.4](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.0...2.15.4)**
* Rimuove lo spellcheck di solr
* Corregge il link al download csv in trasparenza
* Aggiunge codice fiscale, partita iva e codice sdi in footer
* Corregge una vulnerabilità XSS


## [2.0.10](https://gitlab.com/opencontent/openagenda/compare/2.0.9...2.0.10) - 2020-02-28


#### Code dependencies
| Changes                               | From     | To       | Compare                                                                                    |
|---------------------------------------|----------|----------|--------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.133.21 | 3.133.24 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.21...3.133.24)                      |
| opencontent/ocbootstrap-ls            | 1.9.5    | 1.9.6    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.5...1.9.6)                |
| opencontent/openpa_agenda-ls          | 1.24.6   | 1.24.7   | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.6...1.24.7)            |
| opencontent/openpa_bootstrapitalia-ls | 051430e  | 98d712c  | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/051430e...98d712c) |
| psr/log                               | 5628725  | e1cb6ec  | [...](https://github.com/php-fig/log/compare/5628725...e1cb6ec)                            |
| symfony/polyfill-ctype                | fbdeaec  | 4719fa9  | [...](https://github.com/symfony/polyfill-ctype/compare/fbdeaec...4719fa9)                 |
| symfony/polyfill-mbstring             | 34094cf  | 766ee47  | [...](https://github.com/symfony/polyfill-mbstring/compare/34094cf...766ee47)              |
| symfony/polyfill-php73                | 5e66a0f  | 0f27e9f  | [...](https://github.com/symfony/polyfill-php73/compare/5e66a0f...0f27e9f)                 |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.5 and 1.9.6](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.5...1.9.6)**
* Aggiorna le traduzioni in greco

**[opencontent/openpa_agenda-ls changes between 1.24.6 and 1.24.7](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.6...1.24.7)**
* Aggiorna le traduzioni in greco

**[opencontent/openpa_bootstrapitalia-ls changes between 051430e and 98d712c](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/051430e...98d712c)**
* Corregge un bug nel blocco lista per cui non veniva correttamente calcolata la profondità dei contenuti nell'alberatura
* Migliora la visualizzazione da mobile dei factbox
* Permette all'utente loggato di raggiungere la singola immagine da una galleria
* Aggiunge il blocco per esporre un fullcalendar remoto
* Corregge la visualizzazione dei tag nella colonna destra del full


## [2.0.9](https://gitlab.com/opencontent/openagenda/compare/2.0.8...2.0.9) - 2020-02-25


#### Code dependencies
| Changes                               | From     | To       | Compare                                                                                    |
|---------------------------------------|----------|----------|--------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.133.17 | 3.133.21 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.17...3.133.21)                      |
| friendsofsymfony/http-cache           | 1658d8a  | 7560f30  | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/1658d8a...7560f30)          |
| opencontent/ocbootstrap-ls            | 1.9.4    | 1.9.5    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.4...1.9.5)                |
| opencontent/oceditorialstuff-ls       | 2.4.1    | 2.4.2    | [...](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.4.1...2.4.2)           |
| opencontent/ocevents-ls               | 1.1.8    | 1.1.9    | [...](https://github.com/OpencontentCoop/ocevents/compare/1.1.8...1.1.9)                   |
| opencontent/ocopendata-ls             | 2.23.4   | 2.23.5   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.4...2.23.5)               |
| opencontent/ocwebhookserver-ls        | 38bc369  | 32df3cf  | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/38bc369...32df3cf)        |
| opencontent/openpa-ls                 | d3e73a0  | 20665ce  | [...](https://github.com/OpencontentCoop/openpa/compare/d3e73a0...20665ce)                 |
| opencontent/openpa_agenda-ls          | 1.24.5   | 1.24.6   | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.5...1.24.6)            |
| opencontent/openpa_bootstrapitalia-ls | 208a14c  | 051430e  | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/208a14c...051430e) |
| symfony/event-dispatcher              | ec966bd  | 7d4f83e  | [...](https://github.com/symfony/event-dispatcher/compare/ec966bd...7d4f83e)               |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.4 and 1.9.5](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.4...1.9.5)**
* Aggiorna le traduzioni in greco

**[opencontent/oceditorialstuff-ls changes between 2.4.1 and 2.4.2](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.4.1...2.4.2)**
* Aggiorna le traduzioni in greco

**[opencontent/ocevents-ls changes between 1.1.8 and 1.1.9](https://github.com/OpencontentCoop/ocevents/compare/1.1.8...1.1.9)**
* Aggiorna le traduzioni in greco

**[opencontent/ocopendata-ls changes between 2.23.4 and 2.23.5](https://github.com/OpencontentCoop/ocopendata/compare/2.23.4...2.23.5)**
* Hotfix in SearchGateway query with no limitations

**[opencontent/ocwebhookserver-ls changes between 38bc369 and 32df3cf](https://github.com/OpencontentCoop/ocwebhookserver/compare/38bc369...32df3cf)**
* Add hook filter gui

**[opencontent/openpa-ls changes between d3e73a0 and 20665ce](https://github.com/OpencontentCoop/openpa/compare/d3e73a0...20665ce)**
* Coregge un problema nella creazione dell'oranigramma  2874

**[opencontent/openpa_agenda-ls changes between 1.24.5 and 1.24.6](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.5...1.24.6)**
* Aggiorna le traduzioni in greco

**[opencontent/openpa_bootstrapitalia-ls changes between 208a14c and 051430e](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/208a14c...051430e)**
* Corregge un fatal error in OpenPABootstrapItaliaImageUploadHandler
* Corregge le visualizzazioni di una pagina che virtualizza il menu sui tag
* Aggiorna le traduzioni in greco


## [2.0.8](https://gitlab.com/opencontent/openagenda/compare/2.0.7...2.0.8) - 2020-02-19


#### Code dependencies
| Changes                      | From   | To     | Compare                                                                         |
|------------------------------|--------|--------|---------------------------------------------------------------------------------|
| opencontent/openpa_agenda-ls | 1.24.4 | 1.24.5 | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.4...1.24.5) |


Relevant changes by repository:

**[opencontent/openpa_agenda-ls changes between 1.24.4 and 1.24.5](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.4...1.24.5)**
* Genera il pdf con wkhtmltopdf se presente in env
* Corregge un bug di renderizzazione delle date e alcuni problemi di traduzione


## [2.0.7](https://gitlab.com/opencontent/openagenda/compare/2.0.6...2.0.7) - 2020-02-19


#### Code dependencies
| Changes                               | From     | To       | Compare                                                                                    |
|---------------------------------------|----------|----------|--------------------------------------------------------------------------------------------|
| asimlqt/php-google-spreadsheet-client |          | v3.x-dev |                                                                                            |
| aws/aws-sdk-php                       | 3.133.12 | 3.133.17 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.12...3.133.17)                      |
| ezsystems/ezpublish-legacy            | 19701b9  | 855cc50  | [...](https://github.com/Opencontent/ezpublish-legacy/compare/19701b9...855cc50)           |
| guzzlehttp/promises                   | ac2529f  | 89b1a76  | [...](https://github.com/guzzle/promises/compare/ac2529f...89b1a76)                        |
| opencontent/ocbootstrap-ls            | 1.9.3    | 1.9.4    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.3...1.9.4)                |
| opencontent/oceditorialstuff-ls       | 2.4.0    | 2.4.1    | [...](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.4.0...2.4.1)           |
| opencontent/ocevents-ls               | 1.1.7    | 1.1.8    | [...](https://github.com/OpencontentCoop/ocevents/compare/1.1.7...1.1.8)                   |
| opencontent/oci18n                    |          | 0f8551d  |                                                                                            |
| opencontent/ocopendata_forms-ls       | 1.6.2    | 1.6.3    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.2...1.6.3)           |
| opencontent/ocsocialdesign-ls         | 1.6.1    | 1.6.2    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.1...1.6.2)             |
| opencontent/openpa_bootstrapitalia-ls | 323655e  | 208a14c  | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/323655e...208a14c) |
| symfony/deprecation-contracts         | f315c0a  | ede224d  | [...](https://github.com/symfony/deprecation-contracts/compare/f315c0a...ede224d)          |
| symfony/event-dispatcher-contracts    | 5531ac7  | 5a749bd  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5531ac7...5a749bd)     |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.3 and 1.9.4](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.3...1.9.4)**
* Corregge un bug sulle traduzioni di edit ocevents

**[opencontent/oceditorialstuff-ls changes between 2.4.0 and 2.4.1](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.4.0...2.4.1)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/ocevents-ls changes between 1.1.7 and 1.1.8](https://github.com/OpencontentCoop/ocevents/compare/1.1.7...1.1.8)**
* Inserisce le traduzioni in greco

**[opencontent/ocopendata_forms-ls changes between 1.6.2 and 1.6.3](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.2...1.6.3)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/ocsocialdesign-ls changes between 1.6.1 and 1.6.2](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.1...1.6.2)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/openpa_bootstrapitalia-ls changes between 323655e and 208a14c](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/323655e...208a14c)**
* Mostra in website toolbar i link di easyontology qualora presenti
* Aggiorna le traduzioni in greco
* Corregge la paginazione in openpa/roles
* Corregge alcuni problemi di traduzione
* Inserisce la traduzione delle colonne della matrice di opening_hours
* Inserisce il connettore custom per la matrice di opening_hours


## [2.0.6](https://gitlab.com/opencontent/openagenda/compare/2.0.5...2.0.6) - 2020-02-12
- Fix translations

#### Code dependencies
| Changes                               | From    | To       | Compare                                                                                    |
|---------------------------------------|---------|----------|--------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.133.4 | 3.133.12 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.4...3.133.12)                       |
| composer/installers                   | 71a969f | 7d610d5  | [...](https://github.com/composer/installers/compare/71a969f...7d610d5)                    |
| friendsofsymfony/http-cache           | 5abb500 | 1658d8a  | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/5abb500...1658d8a)          |
| opencontent/ocbootstrap-ls            | 1.9.2   | 1.9.3    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.2...1.9.3)                |
| opencontent/ocevents-ls               | 1.1.5   | 1.1.7    | [...](https://github.com/OpencontentCoop/ocevents/compare/1.1.5...1.1.7)                   |
| opencontent/ocexportas-ls             | 1.3.4   | 1.3.5    | [...](https://github.com/OpencontentCoop/ocexportas/compare/1.3.4...1.3.5)                 |
| opencontent/ocmultibinary-ls          | 2.2     | 2.2.1    | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2...2.2.1)                |
| opencontent/openpa-ls                 | 0fffd64 | d3e73a0  | [...](https://github.com/OpencontentCoop/openpa/compare/0fffd64...d3e73a0)                 |
| opencontent/openpa_agenda-ls          | 1.24.2  | 1.24.4   | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.2...1.24.4)            |
| opencontent/openpa_bootstrapitalia-ls | 901bb56 | 323655e  | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/901bb56...323655e) |
| symfony/deprecation-contracts         |         | f315c0a  |                                                                                            |
| symfony/event-dispatcher              | af3ef02 | ec966bd  | [...](https://github.com/symfony/event-dispatcher/compare/af3ef02...ec966bd)               |
| symfony/event-dispatcher-contracts    | 454f462 | 5531ac7  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/454f462...5531ac7)     |
| symfony/options-resolver              | 188abc9 | 1942f69  | [...](https://github.com/symfony/options-resolver/compare/188abc9...1942f69)               |
| symfony/yaml                          | c0b02e2 | 0c7023a  | [...](https://github.com/symfony/yaml/compare/c0b02e2...0c7023a)                           |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.2 and 1.9.3](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.2...1.9.3)**
* Corregge le traduzioni del modulo di cambio password

**[opencontent/ocevents-ls changes between 1.1.5 and 1.1.7](https://github.com/OpencontentCoop/ocevents/compare/1.1.5...1.1.7)**
* Permette l'accesso senza policy per recurrence/parse
* Fix opendata converter

**[opencontent/ocexportas-ls changes between 1.3.4 and 1.3.5](https://github.com/OpencontentCoop/ocexportas/compare/1.3.4...1.3.5)**
* Corregge l'esportazione ANAC dei raggruppamenti

**[opencontent/ocmultibinary-ls changes between 2.2 and 2.2.1](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2...2.2.1)**
* Fix opendata converter

**[opencontent/openpa-ls changes between 0fffd64 and d3e73a0](https://github.com/OpencontentCoop/openpa/compare/0fffd64...d3e73a0)**
* Evoluzione di robots.txt per bloccare i bad crawler
* Aggiunge WhatsApp e Telegram tra i valori possibili dei contatti
* Considera content_tag_menu nel calcolo del tree menu

**[opencontent/openpa_agenda-ls changes between 1.24.2 and 1.24.4](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.2...1.24.4)**
* Corregge alcune traduzioni
* Espone il link corretto ai cookie configurati in agenda root

**[opencontent/openpa_bootstrapitalia-ls changes between 901bb56 and 323655e](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/901bb56...323655e)**
* Evita un possibile errore in indicizzazione dei sottoattributi geo
* Aggiunti temi amaranto e mare ( 1)
* add theme elegance rustico olanda acquamarina ( 2)    add theme amaranto    add amaranto theme    add mare theme    remove unecessary comment    update amaranto theme    add elegance theme    update amaranto    update elegance    add rustico theme for mocheni    add olanda theme    update olanda    add acquamarina theme    update olanda    update acquamarina    update elegance
* Utilizza gli elementi html5 audio e video invece del flash player
* change color to a lighter version
* Possibilità di mostrare l'icona nel tempalte card di default tramite extra parameters Fix minori sui css
* Hot fix link telegram in header social


## [2.0.5](https://gitlab.com/opencontent/openagenda/compare/2.0.4...2.0.5) - 2020-01-22
- Fix ezmbpaex error

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.133.3 | 3.133.4 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.3...3.133.4)             |
| ezsystems/ezmbpaex-ls   | 5.3.3.1 | 5.3.3.3 | [...](https://github.com/Opencontent/ezmbpaex/compare/5.3.3.1...5.3.3.3)        |
| opencontent/ocinstaller | 762f32a | ce5b6e1 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/762f32a...ce5b6e1) |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 762f32a and ce5b6e1](https://github.com/OpencontentCoop/ocinstaller/compare/762f32a...ce5b6e1)**
* Add is_install_from_scratch global variable
* Bug fix  is_install_from_scratch variable
* workaround is_install_from_scratch and variable not found


## [2.0.4](https://gitlab.com/opencontent/openagenda/compare/2.0.3...2.0.4) - 2020-01-21
- Add default demo recaptcha keys
- Increase the default password lifetime
- Fix Agenda Admin role assignment in installer

#### Code dependencies
| Changes                               | From    | To      | Compare                                                                                    |
|---------------------------------------|---------|---------|--------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.129.1 | 3.133.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.129.1...3.133.3)                        |
| friendsofsymfony/http-cache           | 3c1cb34 | 5abb500 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/3c1cb34...5abb500)          |
| guzzlehttp/promises                   | 17d36ed | ac2529f | [...](https://github.com/guzzle/promises/compare/17d36ed...ac2529f)                        |
| mtdowling/jmespath.php                | 2.4.0   | 52168cb | [...](https://github.com/jmespath/jmespath.php/compare/2.4.0...52168cb)                    |
| opencontent/ocbootstrap-ls            | 1.8.1   | 1.9.2   | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.8.1...1.9.2)                |
| opencontent/ocevents-ls               | 1.1.3   | 1.1.5   | [...](https://github.com/OpencontentCoop/ocevents/compare/1.1.3...1.1.5)                   |
| opencontent/ocexportas-ls             | 1.3.3   | 1.3.4   | [...](https://github.com/OpencontentCoop/ocexportas/compare/1.3.3...1.3.4)                 |
| opencontent/ocgdprtools-ls            | 1.4.2   | 1.4.5   | [...](https://github.com/OpencontentCoop/ocgdprtools/compare/1.4.2...1.4.5)                |
| opencontent/ocinstaller               | 598670e | 762f32a | [...](https://github.com/OpencontentCoop/ocinstaller/compare/598670e...762f32a)            |
| opencontent/ocopendata-ls             | 2.23.3  | 2.23.4  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.3...2.23.4)               |
| opencontent/ocopendata_forms-ls       | 1.6.0   | 1.6.2   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.0...1.6.2)           |
| opencontent/ocsocialdesign-ls         | 1.6.0   | 1.6.1   | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.0...1.6.1)             |
| opencontent/ocsocialuser-ls           | 1.8.0   | 1.8.1   | [...](https://github.com/OpencontentCoop/ocsocialuser/compare/1.8.0...1.8.1)               |
| opencontent/openpa-ls                 | 12d6d5f | 0fffd64 | [...](https://github.com/OpencontentCoop/openpa/compare/12d6d5f...0fffd64)                 |
| opencontent/openpa_bootstrapitalia-ls | 7d73f75 | 901bb56 | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/7d73f75...901bb56) |
| opencontent/openpa_theme_2014-ls      | 2.14.1  | 2.15.0  | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.14.1...2.15.0)        |
| php-http/client-common                | 0e6a80e | 03086a2 | [...](https://github.com/php-http/client-common/compare/0e6a80e...03086a2)                 |
| php-http/discovery                    | 6a5cb62 | 82dbef6 | [...](https://github.com/php-http/discovery/compare/6a5cb62...82dbef6)                     |
| php-http/guzzle6-adapter              | 4b491b7 | bd7b405 | [...](https://github.com/php-http/guzzle6-adapter/compare/4b491b7...bd7b405)               |
| php-http/httplug                      | ec4c56a | c9b2424 | [...](https://github.com/php-http/httplug/compare/ec4c56a...c9b2424)                       |
| php-http/message                      | d9f6bf6 | fcf9b45 | [...](https://github.com/php-http/message/compare/d9f6bf6...fcf9b45)                       |
| php-http/promise                      | a339534 | 02ee67f | [...](https://github.com/php-http/promise/compare/a339534...02ee67f)                       |
| symfony/event-dispatcher              | 9c93df8 | af3ef02 | [...](https://github.com/symfony/event-dispatcher/compare/9c93df8...af3ef02)               |
| symfony/event-dispatcher-contracts    | af23c25 | 454f462 | [...](https://github.com/symfony/event-dispatcher-contracts/compare/af23c25...454f462)     |
| symfony/options-resolver              | 9a47735 | 188abc9 | [...](https://github.com/symfony/options-resolver/compare/9a47735...188abc9)               |
| symfony/polyfill-ctype                | f8f0b46 | fbdeaec | [...](https://github.com/symfony/polyfill-ctype/compare/f8f0b46...fbdeaec)                 |
| symfony/polyfill-mbstring             |         | 34094cf |                                                                                            |
| symfony/polyfill-php73                | 4b0e222 | 5e66a0f | [...](https://github.com/symfony/polyfill-php73/compare/4b0e222...5e66a0f)                 |
| symfony/yaml                          | a79900f | c0b02e2 | [...](https://github.com/symfony/yaml/compare/a79900f...c0b02e2)                           |
| zetacomponents/mail                   | 1.8.4   | 1.9.1   | [...](https://github.com/zetacomponents/Mail/compare/1.8.4...1.9.1)                        |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.8.1 and 1.9.2](https://github.com/OpencontentCoop/ocbootstrap/compare/1.8.1...1.9.2)**
* Introduce la funzionalità dei moduli di login per cui è possibile aggiungere un modulo alla pagina /user/login da un'estensione terza
* Permette l'inserimento di testo formattato nei moduli di login
* Recepisce gli aggiornamenti di ocevents

**[opencontent/ocevents-ls changes between 1.1.3 and 1.1.5](https://github.com/OpencontentCoop/ocevents/compare/1.1.3...1.1.5)**
* Implementa fromString Verifica il popolamento se il campo è obbligatorio Corregge la visualizzazione di edit da backend Corregge alcuni bug di visualizzazione Impone anche DTEND in rrule.js

**[opencontent/ocexportas-ls changes between 1.3.3 and 1.3.4](https://github.com/OpencontentCoop/ocexportas/compare/1.3.3...1.3.4)**
* Aggiorna exporter ANAC sulla base di http://www.anticorruzione.it/portal/public/classic/Servizi/ServiziOnline/DichiarazioneAdempLegge190

**[opencontent/ocgdprtools-ls changes between 1.4.2 and 1.4.5](https://github.com/OpencontentCoop/ocgdprtools/compare/1.4.2...1.4.5)**
* Corregge la visualizzazione del datatype in versione collectinfo
* Corregge il connettore opendata_form per il caso in cui l'editor non sia l'utente corrente
* Svuota la cache statica (varnish) al cambio di valore di accettazione (varnish mette in cache anche i 302)
* Ignora i privilegi di accesso al modulo gdpr/user_acceptance

**[opencontent/ocinstaller changes between 598670e and 762f32a](https://github.com/OpencontentCoop/ocinstaller/compare/598670e...762f32a)**
* Log class compare details
* Add condition parameter
* Get env vars from $_SERVER

**[opencontent/ocopendata-ls changes between 2.23.3 and 2.23.4](https://github.com/OpencontentCoop/ocopendata/compare/2.23.3...2.23.4)**
* Fix temp file creation in exportas paginated download

**[opencontent/ocopendata_forms-ls changes between 1.6.0 and 1.6.2](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.0...1.6.2)**
* Corregge la visualizzazione delle relazioni in formato checkbox con un workaround per evitare un bug di alpacajs
* Corregge alcuni piccoli bug di visualizzazione

**[opencontent/ocsocialdesign-ls changes between 1.6.0 and 1.6.1](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.0...1.6.1)**
* Eredita il template di login da ocbootstrap

**[opencontent/ocsocialuser-ls changes between 1.8.0 and 1.8.1](https://github.com/OpencontentCoop/ocsocialuser/compare/1.8.0...1.8.1)**
* Corregge un bug su stringa di traduzione

**[opencontent/openpa-ls changes between 12d6d5f and 0fffd64](https://github.com/OpencontentCoop/openpa/compare/12d6d5f...0fffd64)**
* Fix create_instance script
* add agenda in OpenPABase::getInstances

**[opencontent/openpa_bootstrapitalia-ls changes between 7d73f75 and 901bb56](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/7d73f75...901bb56)**
* Aumenta il limite massimo di ricerca in opendata content env per permettere il corretto funzionamento dell'interfaccia di openpa role

**[opencontent/openpa_theme_2014-ls changes between 2.14.1 and 2.15.0](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.14.1...2.15.0)**
* Eredita il template di login da ocbootstrap e recepisce la funzionalità login_modules di ocbootstrap


## [2.0.3](https://gitlab.com/opencontent/openagenda/compare/2.0.2...2.0.3) - 2019-12-17
- Fix image.ini and composer deps

#### Code dependencies
| Changes                               | From    | To      | Compare                                                                                    |
|---------------------------------------|---------|---------|--------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                       | 3.129.0 | 3.129.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.129.0...3.129.1)                        |
| opencontent/ocevents-ls               | 1.1.2   | 1.1.3   | [...](https://github.com/OpencontentCoop/ocevents/compare/1.1.2...1.1.3)                   |
| opencontent/openpa_agenda-ls          | 1.24.1  | 1.24.2  | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.1...1.24.2)            |
| opencontent/openpa_bootstrapitalia-ls | 584fc7a | 7d73f75 | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/584fc7a...7d73f75) |
| php-http/discovery                    | 5a0da02 | 6a5cb62 | [...](https://github.com/php-http/discovery/compare/5a0da02...6a5cb62)                     |
| php-http/httplug                      | e7bbae4 | ec4c56a | [...](https://github.com/php-http/httplug/compare/e7bbae4...ec4c56a)                       |
| php-http/message                      | 144d13b | d9f6bf6 | [...](https://github.com/php-http/message/compare/144d13b...d9f6bf6)                       |


Relevant changes by repository:

**[opencontent/ocevents-ls changes between 1.1.2 and 1.1.3](https://github.com/OpencontentCoop/ocevents/compare/1.1.2...1.1.3)**
* Hotfix in recurrence  translator loader

**[opencontent/openpa_agenda-ls changes between 1.24.1 and 1.24.2](https://github.com/OpencontentCoop/openpa_agenda/compare/1.24.1...1.24.2)**
* Corregge una traduzione mancante

**[opencontent/openpa_bootstrapitalia-ls changes between 584fc7a and 7d73f75](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/584fc7a...7d73f75)**
* Corregge una traduzione mancante


## [2.0.2](https://gitlab.com/opencontent/openagenda/compare/2.0.1...2.0.2) - 2019-12-16
- Update changelog

#### Code dependencies
| Changes                               | From    | To      | Compare                                                                                    |
|---------------------------------------|---------|---------|--------------------------------------------------------------------------------------------|
| opencontent/ocbootstrap-ls            | 1.7.6   | 1.8.1   | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.7.6...1.8.1)                |
| opencontent/oceditorialstuff-ls       | 2.3.2   | 2.4.0   | [...](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.3.2...2.4.0)           |
| opencontent/ocopendata_forms-ls       | 1.5.3   | 1.6.0   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.5.3...1.6.0)           |
| opencontent/ocsocialdesign-ls         | 1.5.1.0 | 1.6.0   | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.5.1.0...1.6.0)           |
| opencontent/ocsocialuser-ls           | 1.7.2.0 | 1.8.0   | [...](https://github.com/OpencontentCoop/ocsocialuser/compare/1.7.2.0...1.8.0)             |
| opencontent/openpa_agenda-ls          | 1.23.9  | 1.24.1  | [...](https://github.com/OpencontentCoop/openpa_agenda/compare/1.23.9...1.24.1)            |
| opencontent/openpa_bootstrapitalia-ls | c152295 | 584fc7a | [...](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/c152295...584fc7a) |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.7.6 and 1.8.1](https://github.com/OpencontentCoop/ocbootstrap/compare/1.7.6...1.8.1)**
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge il file di traduzione delle stringhe in greco

**[opencontent/oceditorialstuff-ls changes between 2.3.2 and 2.4.0](https://github.com/OpencontentCoop/oceditorialstuff/compare/2.3.2...2.4.0)**
* Aggiunge il file di traduzione delle stringhe in greco

**[opencontent/ocopendata_forms-ls changes between 1.5.3 and 1.6.0](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.5.3...1.6.0)**
* Aggiunge il file di traduzione delle stringhe in greco

**[opencontent/ocsocialdesign-ls changes between 1.5.1.0 and 1.6.0](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.5.1.0...1.6.0)**
* Aggiunge il file di traduzione delle stringhe in greco

**[opencontent/ocsocialuser-ls changes between 1.7.2.0 and 1.8.0](https://github.com/OpencontentCoop/ocsocialuser/compare/1.7.2.0...1.8.0)**
* Aggiunge il file di traduzione delle stringhe in greco

**[opencontent/openpa_agenda-ls changes between 1.23.9 and 1.24.1](https://github.com/OpencontentCoop/openpa_agenda/compare/1.23.9...1.24.1)**
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge il file di traduzione delle stringhe in greco

**[opencontent/openpa_bootstrapitalia-ls changes between c152295 and 584fc7a](https://github.com/OpencontentCoop/openpa_bootstrapitalia/compare/c152295...584fc7a)**
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge il file di traduzione delle stringhe in greco


## [2.0.1](https://gitlab.com/opencontent/openagenda/compare/2.0.0...2.0.1) - 2019-12-16
- Update traefik Docker tag to v1.7.20 [`#18`](https://gitlab.com/opencontent/openagenda/merge_requests/18)
- Updating aws/aws-sdk-php (3.128.1 =&gt; 3.129.0) [`a2ada08`](https://gitlab.com/opencontent/openagenda/commit/a2ada08f2b0cfeec05affa40a1b783b619c49bc1)

## [2.0.0](https://gitlab.com/opencontent/openagenda/compare/1.21.1...2.0.0) - 2019-12-12
- Added codeowners [`#17`](https://gitlab.com/opencontent/openagenda/merge_requests/17)
- Added build of nginx and solr images to reduce configuration files needed for a complete environment to a single docker-compose.yml file [`#16`](https://gitlab.com/opencontent/openagenda/merge_requests/16)
- Upgrade to bootstrapitalia content architecture and design [`#15`](https://gitlab.com/opencontent/openagenda/merge_requests/15)
- Update dependency ezsystems/eztags-ls to v2.2.2 [`#12`](https://gitlab.com/opencontent/openagenda/merge_requests/12)
- Added automatic api tests to ci [`#14`](https://gitlab.com/opencontent/openagenda/merge_requests/14)
- Configure Renovate [`#10`](https://gitlab.com/opencontent/openagenda/merge_requests/10)
- Fix(publiccode): the openagenda software is now correctly managed under the repository of Comune di Ala [`#9`](https://gitlab.com/opencontent/openagenda/merge_requests/9)
- Check validity of publiccode.yml [`#8`](https://gitlab.com/opencontent/openagenda/merge_requests/8)
- Updated ZAP progress file [`#7`](https://gitlab.com/opencontent/openagenda/merge_requests/7)
- Add renovate.json [`16ae408`](https://gitlab.com/opencontent/openagenda/commit/16ae408d449dc98e1cdc7615030c0fafe8160458)
- Add screenshots [`51264ba`](https://gitlab.com/opencontent/openagenda/commit/51264baf024944beeaf57e49d67b833c3420ed26)

## [1.21.1](https://gitlab.com/opencontent/openagenda/compare/first...1.21.1) - 2019-07-28
- Add logo in folder and ref in file
- Add screenshot folder and ref in publiccode
- Shorten feature description
- Added chown of solr files
- Fixed solr config
- Added info for a correct production deployment
- Fixed error in container names
- Added credentials for the default user
- Added copyright notice
- Added License tag
- Added security file
- Added healthchecks
- Changed copyright from Opencontent to Comune di Ala, that release the software for reuse
- More updates to publish the repo for reuse
- Cleanup
- Fixing dependency problem in CI: libicu57 is not available now, but is a dependency of libicu-dev so I simply drop it
- Update publiccode.yml
- add default prototipo settings
- CI: removed DAST security check (not available in free profile) and added Owasp ZAP execution (non blocking)
- Added GPL V2 License
- dump sql
- Update .gitlab-ci.yml: changed region to Ireland
- add ci conf

