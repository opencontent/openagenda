<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=
Port=
User=
Password=
Database=
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[FileSettings]
VarDir=

[SiteSettings]
SiteName=
SiteURL=
DefaultAccess=ita_agenda
SiteList[]
SiteList[]=agenda
SiteList[]=ita_agenda
SiteList[]=backend

[HTTPHeaderSettings]
CustomHeader=enabled
OnlyForAnonymous=disabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=60, s-maxage=600
HeaderList[]=Vary
Vary[/]=X-User-Context-Hash

[VarnishSettings]
VarnishHostName=
VarnishPort=
VarnishServers[]

[SearchSettings]
LogSearchStats=disabled

[RoleSettings]
PolicyOmitList[]=user/do_logout
PolicyOmitList[]=exportas/csv
PolicyOmitList[]=exportas/custom
PolicyOmitList[]=ezinfo/is_alive
PolicyOmitList[]=opendata/api

[Session]
SessionNameHandler=custom
CookieSecure=true
CookieHttponly=true
Handler=ezpSessionHandlerPHP
ForceStart=disabled

[InformationCollectionSettings]
EmailReceiver=

[UserSettings]
LogoutRedirect=/?logout
RegistrationEmail=
MaxNumberOfFailedLogin=10

[SiteAccessSettings]
ForceVirtualHost=true
DebugAccess=enabled
DebugExtraAccess=enabled
CheckValidity=false
MatchOrder=uri
HostMatchMapItems[]
RelatedSiteAccessList[]
RelatedSiteAccessList[]=agenda
RelatedSiteAccessList[]=ita_agenda
RelatedSiteAccessList[]=backend
AvailableSiteAccessList[]
AvailableSiteAccessList[]=backend
AvailableSiteAccessList[]=ita_agenda
AvailableSiteAccessList[]=agenda

[MailSettings]
TransportAlias[smtp]=OpenPASMTPTransport
Transport=smtp
TransportConnectionType=tls
TransportServer=
TransportPort=
TransportUser=
TransportPassword=
AdminEmail=
EmailSender=
SenderHost=
BlackListEmailDomains[]
BlackListEmailDomainSuffixes[]

[EmbedViewModeSettings]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]=embed-inline

[TimeZoneSettings]
TimeZone=Europe/Rome

[RegionalSettings]
TextTranslation=enabled

[ContentSettings]
ContentObjectNameLimit=203
TranslationList=

[DebugSettings]
DebugToolbar=disabled

[UserFormToken]
CookieHttponly=true
CookieSecure=1

[Event]
Listeners[]=content/download@OpenPADownloadFilter::addXRobotsTagHeader

*/ ?>
