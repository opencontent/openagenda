<?php /*

[PushToBlock]
ContentClasses[]
RootSubtree=1

[General]
AllowedTypes[]
AllowedTypes[]=ListaAutomatica
AllowedTypes[]=ListaManuale

[Singolo]
Name=Oggetto singolo
NumberOfValidItems=1
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
ViewList[]
ViewList[]=default
ViewList[]=card
ViewList[]=card_image
ViewList[]=card_children
ViewName[]
ViewName[default]=Default
ViewName[card_image]=Card (solo immagine)
#ViewName[card]=Card
#ViewName[card_children]=Card (ultimi 4 contenuti)
ItemsPerRow[]
ContainerStyle[]
#ContainerStyle[card]=py-5
#ContainerStyle[card_children]=py-5
Wide[]
Wide[]=card_image

[ListaAutomatica]
Name=Lista automatica
NumberOfValidItems=1
NumberOfArchivedItems=0
CustomAttributes[]
CustomAttributes[]=node_id
UseBrowseMode[node_id]=true
CustomAttributes[]=limite
CustomAttributes[]=elementi_per_riga
CustomAttributes[]=includi_classi
CustomAttributes[]=escludi_classi
CustomAttributes[]=ordinamento
CustomAttributes[]=livello_profondita
CustomAttributes[]=state_id
CustomAttributes[]=topic_node_id
CustomAttributeNames[]
CustomAttributeNames[livello_profondita]=Livello di profondità nell'alberatura
CustomAttributeNames[limite]=Numero di elementi
CustomAttributeNames[elementi_per_riga]=Elementi per riga
CustomAttributeNames[includi_classi]=Tipologie di contenuto da includere
CustomAttributeNames[escludi_classi]=Tipologie di contenuto da escludere (alternativo rispetto al precedente)
CustomAttributeNames[ordinamento]=Ordina per
CustomAttributeNames[state_id]=Stato
CustomAttributeNames[topic_node_id]=Argomenti
CustomAttributeTypes[elementi_per_riga]=select
CustomAttributeSelection_elementi_per_riga[]
CustomAttributeSelection_elementi_per_riga[unset]=Non specificato
CustomAttributeSelection_elementi_per_riga[2]=2
CustomAttributeSelection_elementi_per_riga[3]=3
CustomAttributeSelection_elementi_per_riga[4]=4
CustomAttributeSelection_elementi_per_riga[6]=6
CustomAttributeSelection_elementi_per_riga[auto]=Masonry
CustomAttributeTypes[ordinamento]=select
CustomAttributeTypes[includi_classi]=class_select
CustomAttributeTypes[escludi_classi]=class_select
CustomAttributeSelection_ordinamento[]
CustomAttributeSelection_ordinamento[name]=Titolo
CustomAttributeSelection_ordinamento[pubblicato]=Data di pubblicazione
CustomAttributeSelection_ordinamento[modificato]=Data di ultima modifica
CustomAttributeSelection_ordinamento[priority]=Priorità del nodo
CustomAttributeTypes[state_id]=state_select
CustomAttributeTypes[topic_node_id]=topic_select
ManualAddingOfItems=disabled
ViewList[]
ViewList[]=lista_card
ViewList[]=lista_card_alt
ViewList[]=lista_card_image
ViewList[]=lista_card_children
ViewList[]=lista_accordion
ViewList[]=lista_banner
#ViewList[]=lista_carousel
ViewName[]
ViewName[lista_card]=Card
ViewName[lista_card_alt]=Card (alternativa)
ViewName[lista_card_image]=Card (solo immagine)
ViewName[lista_card_children]=Card (ultimi 4 contenuti)
ViewName[lista_accordion]=Accordion
ViewName[lista_banner]=Banner
#ViewName[lista_carousel]=Carousel
TTL=3600
ItemsPerRow[]
ContainerStyle[]
ContainerStyle[lista_card]=py-5
ContainerStyle[lista_card_alt]=py-5
ContainerStyle[lista_card_image]=py-5
ContainerStyle[lista_card_children]=py-5
ContainerStyle[lista_accordion]=py-5
ContainerStyle[lista_banner]=py-5
ContainerStyle[lista_carousel]=py-5

[ListaManuale]
Name=Lista manuale
NumberOfValidItems=15
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
CustomAttributes[]
CustomAttributes[]=elementi_per_riga
CustomAttributeNames[]
CustomAttributeNames[elementi_per_riga]=Elementi per riga
CustomAttributeTypes[elementi_per_riga]=select
CustomAttributeSelection_elementi_per_riga[]
CustomAttributeSelection_elementi_per_riga[unset]=Non specificato
CustomAttributeSelection_elementi_per_riga[2]=2
CustomAttributeSelection_elementi_per_riga[3]=3
CustomAttributeSelection_elementi_per_riga[4]=4
CustomAttributeSelection_elementi_per_riga[6]=6
CustomAttributeSelection_elementi_per_riga[auto]=Masonry
ViewList[]
ViewList[]=lista_card
ViewList[]=lista_card_alt
ViewList[]=lista_card_image
ViewList[]=lista_card_children
ViewList[]=lista_accordion
ViewList[]=lista_banner
#ViewList[]=lista_carousel
ViewName[]
ViewName[lista_card]=Card
ViewName[lista_card_alt]=Card (alternativa)
ViewName[lista_card_image]=Card (solo immagine)
ViewName[lista_card_children]=Card (ultimi 4 contenuti)
ViewName[lista_accordion]=Accordion
ViewName[lista_banner]=Banner
#ViewName[lista_carousel]=Carousel
ItemsPerRow[]
ContainerStyle[]
ContainerStyle[lista_card]=py-5
ContainerStyle[lista_card_alt]=py-5
ContainerStyle[lista_card_image]=py-5
ContainerStyle[lista_card_children]=py-5
ContainerStyle[lista_accordion]=py-5
ContainerStyle[lista_banner]=py-5
ContainerStyle[lista_carousel]=py-5


*/ ?>
